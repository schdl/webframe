<?php
namespace WebFrame;
abstract class Core {
	public static $root;
	public static $ini_files;
	public static $modules;
	public static $start;
	public static $use_templates=true;
	public static $request;
	public static $page=null;
	public static $scripts=[];
	public static $debug=[];
	public static $user;
	public static $db;
	public static $cache;
	public static $context;
	public static $conf=[];
	public static $conf_descr=[];
	public static $ajax=false;
	public static $session;
	public static $template_state=[];
	public static $awaiting_footer;
	public static function init() {
		date_default_timezone_set('UTC');
		self::$start=microtime(true);
		self::$root=realpath(__DIR__.'/..');
		foreach (glob(self::$root.'/functions/*.php') as $file) {
			require_once($file);
		}
		self::readINI();
		self::loadFunctions();
		self::loadCache();
		spl_autoload_register('WebFrame\\autoloader');
		debug(__METHOD__, 'module conf: '.var_export(self::$modules, true));
		if (!isCLI()) {
			set_exception_handler('WebFrame\\exception_handler');
			set_error_handler('WebFrame\\error_handler');
			register_shutdown_function('WebFrame\\onshutdown');
		}
		self::readConfig();
		if (isset($_SERVER['REQUEST_URI'])) {
			$uri=$_SERVER['REQUEST_URI'];
			if (($pos=strpos($uri, '?')) !== false) {
				$uri=substr($uri, 0, $pos);
			}
			$path=getConf('http_path');
			if (substr($uri, 0, strlen($path)) !== $path) {
				throw new \RuntimeException('URI '.$uri.' is not in the path ('.$path.')');
			}
			self::$request=substr($uri, strlen($path));
		}
		if (isset(self::$cache['initializers'])) {
			foreach (self::$cache['initializers'] as $init) {
				$init['callback']();
			}
		}
	}
	private static function readINI() {
		self::$ini_files=glob(self::$root.'/*/WebFrame.ini', GLOB_NOSORT);
		foreach (self::$ini_files as $cf) {
			$root=dirname($cf);
			$defaults=array(
				'root' => $root,
				'ini_file' => $cf,
				'module_name' => basename($root),
				'precedence' => 50,
				'class_path' => 'classes:pages',
				'function_path' => 'functions',
				'css_path' => 'css',
				'static_path' => 'static',
				'template_dir' => 'templates',
				'scripts' => ''
			);
			$conf=array_merge($defaults, parse_ini_file($cf, true, \INI_SCANNER_NORMAL));
			$root=$conf['root'];
			$module=$conf['module_name'];
			self::$modules[$module]=$conf;
			if (strlen(trim($conf['scripts'])) > 0) {
				foreach (explode(',', trim($conf['scripts'])) as $script) {
					self::$scripts[]=trim($script);
				}
			}
		}
		// prioritize function has not yet been loaded
		uasort(self::$modules, function($a,$b) {
			if ($a['precedence'] > $b['precedence']) {
				return -1;
			} elseif ($a['precedence'] < $b['precedence']) {
				return 1;
			} else {
				return 0;
			}
		});
		$defaults=array_reverse(self::$modules);
		foreach ($defaults as $module => $conf) {
			if (isset($conf['defaults'])) {
				self::$conf=array_merge(self::$conf, $conf['defaults']);
			}
		}
		sort(self::$scripts);
	}
	private static function loadFunctions() {
		// Manually load WebFrame's functions because they are needed for loading other modules' functions
		foreach (glob(self::$modules['WebFrame']['root'].'/'.self::$modules['WebFrame']['function_path'].'/*.php') as $file) {
			require_once($file);
		}
		$files=[];
		foreach (modules() as $module) {
			$dir=functionPath($module);
			if (isset($dir)) {
				foreach (glob($dir.'/*.php') as $file) {
					$files[]=$file;
				}
			}
		}
		foreach ($files as $file) {
			require_once($file);
		}
	}
	private static function loadCache() {
		if (is_file(rootDir().'/Cache.json')) {
			self::$cache=json_decode(file_get_contents(rootDir().'/Cache.json'), true);
		}
	}
	private static function readConfig() {
		debug('defaults', var_export(self::$conf, true));
		require(rootDir().'/config.php');
		self::$conf=array_merge(self::$conf, get_defined_vars());
		debug('config', var_export(self::$conf, true));
	}
	// Only used by user()
	public static function initSession() {
		$cookiename=getConf('cookiename');
		if (isset($_COOKIE[$cookiename])) {
			$cookie=$_COOKIE[$cookiename];
			debug('session', 'cookie received '.$cookiename.'='.$cookie);
			$session=session::select_by_id($cookie)->fetch();
			if ($session) {
				if ($session->atime+$session->expire < time()) {
					setcookie($cookiename);
					$session->delete();
					debug('session', 'session was expired');
				} else {
					$session->atime=time();
					$session->write();
					self::$user=$session->get_user();
					debug('session', 'sessionid='.$session->id.', email='.user()->email);
					self::$session=$session;
					if (!headers_sent()) {
						setcookie($cookiename, $session->id, time()+getConf('sessionlength'));
					}
					if (user()->has_flag('a') && isset($_COOKIE[$cookiename.'_debug']) && $_COOKIE[$cookiename.'_debug']) {
						setConf('debug', true);
					}
					if (getConf('allow_su') && user()->has_flag('a') && isset($_COOKIE[$cookiename.'_su'])) {
						if ($_COOKIE[$cookiename.'_su'] == 'null') {
							self::$user=null;
						} else {
							$user=user::select_by_id($_COOKIE[$cookiename.'_su'])->fetch();
							if ($user) {
								self::$user=$user;
							}
						}
					}
				}
			} else {
				setcookie($cookiename);
				debug('session', 'session not found');
			}
		} else {
			debug('session', 'no cookie received for '.$cookiename);
		}
	}
}
Core::init();
?>
