#!/usr/bin/env php
<?php
namespace WebFrame;
require_once(__DIR__.'/../Core.php');
foreach (modules() as $mod) {
	if (is_file(moduleRoot($mod).'/DBStructure.json')) {
		unlink(moduleRoot($mod).'/DBStructure.json');
	}
	foreach (classPath($mod) as $cp) {
		if (!is_dir($cp)) {
			continue;
		}
		foreach (glob($cp.'/*.php') as $file) {
			if (is_file($file)) {
				require_once($file);
			}
		}
	}
}
$modules=array();
$tables=array();
foreach (get_declared_classes() as $class) {
	if (!is_subclass_of($class, 'DB\\Row')) {
		continue;
	}
	$table=$class::tableName();
	list($module, $class)=explode('\\', $class, 2);
	$modules[$module][]=$table;
	if (!isset($tables[$table])) {
		$tables[$table]=\DB\Helper::scan_table($table);
	}
}
foreach ($modules as $module => $table_names) {
	echo $module.' ('.implode(', ', $table_names).')'."\n";
	$data=array();
	foreach ($table_names as $name) {
		$data[$name]=$tables[$name];
	}
	$file=fopen(moduleRoot($module).'/DBStructure.json', 'w');
	fputs($file, json_encode($data, \JSON_PRETTY_PRINT)."\n");
	fclose($file);
}
$r=query_db('SHOW TABLES');
$unused=array();
while (($table=$r->fetch(\PDO::FETCH_COLUMN)) !== false) {
	if (!isset($tables[$table])) {
		$unused[]=$table;
	}
}
if ($unused) {
	echo 'Unused tables: '.implode(', ', $unused)."\n";
}
?>
