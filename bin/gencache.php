#!/usr/bin/env php
<?php
namespace WebFrame;
$file=__DIR__.'/../../Cache.json';
if (is_file($file)) {
	unlink(__DIR__.'/../../Cache.json');
}
require(__DIR__.'/../Core.php');
Core::$cache=[];
// We need to be able to autoload classes because inheritance triggers the autoloader
spl_autoload_register('WebFrame\\bootstrap_autoloader');
$modules=[];
foreach (modules() as $module) {
	$modules[$module]=true;
	foreach (classPath($module) as $dir) {
		$it=new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dir, \FilesystemIterator::FOLLOW_SYMLINKS));
		for (; $it->valid(); $it->next()) {
			if ($it->isDot() || $it->isDir() || !$it->isReadable() || strtolower($it->getExtension()) != 'php') {
				continue;
			}
			require_once($dir.'/'.$it->getSubPathname());
		}
	}
}
// Check all classes
foreach (array_merge(get_declared_classes(), get_declared_interfaces(), get_declared_traits()) as $class) {
	$rc=new \ReflectionClass($class);
	// Ignore classes not in a WebFrame module
	if (!$rc->inNamespace() || !array_key_exists($rc->getNamespaceName(), $modules)) {
		continue;
	}
	// Cache location of classes for use by the autoloader
	Core::$cache['autoloader'][$class]=$rc->getFileName();
	// Continue unless we're dealing with a class that has a WebFrame method
	if ($rc->isInterface() || $rc->isTrait() || !$rc->hasMethod('WebFrame')) {
		continue;
	}
	// Check that WebFrame is static and not public
	$rf=$rc->getMethod('WebFrame');
	if (!$rf->isStatic()) {
		echo 'Error: '.$class.'\\WebFrame() must be static!'."\n";
		continue;
	}
	if ($rf->isPublic()) {
		echo 'Warning: '.$class.'\\WebFrame() should not be public!'."\n";
	} else {
		// Allow us to call WebFrame
		$rf->setAccessible(true);
	}
	// Set context
	Core::$context=$class;
	// Invoke $class::WebFrame();
	$rf->invoke(null, $rc);
}
// Check all modules for a WebFrame function
foreach (modules() as $module) {
	if (function_exists($func=$module.'\\WebFrame')) {
		$context=$module;
		$func();
	}
}
foreach (Core::$cache as &$items) {
	prioritize($items);
}
if (isset(Core::$cache['registrars'])) {
	foreach (Core::$cache['registrars'] as $registrar) {
		$registrar['callback']();
	}
}
file_put_contents(rootDir().'/Cache.json', json_encode(Core::$cache, \JSON_PRETTY_PRINT|\JSON_UNESCAPED_SLASHES));
?>
