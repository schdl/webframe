#!/usr/bin/env php
<?php
namespace WebFrame;
require(__DIR__.'/../Core.php');
$defaults=array();
$descr=array();
$confs=array();
foreach (Core::$ini_files as $cf) {
	$confs[]=parse_ini_file($cf, true, INI_SCANNER_RAW);
}
prioritize($confs, false);
foreach ($confs as $conf) {
	if (isset($conf['defaults'])) {
		$defaults=array_merge($defaults, $conf['defaults']);
	}
	if (isset($conf['config_descriptions'])) {
		$descr=array_merge($descr, $conf['config_descriptions']);
	}
}
copy(rootDir().'/config.php', rootDir().'/config.php.old');
$file=fopen(rootDir().'/config.php', 'w');
fwrite($file, '<?php'."\n");
foreach ($defaults as $name => $val) {
	$val=stringify_val($val);
	$comment=(isset($descr[$name])?$descr[$name]:$name).' (default='.$val.'):';
	$comment=explode("\n", wordwrap($comment, 72, "\n", false));
	for ($i=0; $i<count($comment); $i++) {
		$line=$comment[$i];
		if ($i == 0) {
			$line='/* '.$line;
		} else {
			$line=' * '.$line;
		}
		if ($i+1 == count($comment)) {
			$line.=' */';
		}
		fwrite($file, $line."\n");
	}
	fwrite($file, '$'.$name.' = '.var_export(getConf($name), true).";\n");
}
fwrite($file, '?>'."\n");
fclose($file);
chmod(rootDir().'/config.php', 0640);
function stringify_val($val) {
	static $translate=array('yes'=>true, 'true'=>true, 'null'=>null, 'no'=>false, 'false'=>false);
	if (is_string($val)) {
		if (array_key_exists(strtolower($val), $translate)) {
			$val=$translate[strtolower($val)];
		}
		if (is_numeric($val)) {
			$val=$val+0;
		}
		$val=var_export($val, true);
	} elseif (is_array($val)) {
		$vals=array();
		foreach ($val as $name => $v) {
			$vals[]=(is_numeric($name)?'':var_export($name, true).' => ').stringify_val($v);
		}
		$val='array('.implode(',', $vals).')';
	} else {
		$val=var_export($val, true);
	}
	return $val;
}
?>
