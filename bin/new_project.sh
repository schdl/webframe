#!/bin/bash
WFROOT="$(readlink -e $(dirname ${0})/..)"
CUR=$(readlink -e "$PWD")
REL=${WFROOT#"$CUR/"}
read -p "Create new module in $CUR with links to $CUR/$REL? " -n 1
echo ""
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    exit 1
fi
set -x
mkdir NewModule
echo "module_name=NewModule" > NewModule/WebFrame.ini
mkdir NewModule/classes
mkdir NewModule/css
mkdir NewModule/functions
mkdir NewModule/pages
mkdir NewModule/static
mkdir NewModule/static/js
mkdir NewModule/static/images
touch NewModule/50_routing
"$WFROOT/bin/genconfig.php" > config.php
ln -sv "$REL/htaccess" .htaccess
ln -sv -t . "$REL/handler.php"
