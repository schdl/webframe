#!/usr/bin/env php
<?php
namespace WebFrame;
require(__DIR__.'/../Core.php');
if (isset($argv[1])) {
	$email=$argv[1];
} else {
	echo 'E-mail: ';
	$email=trim(fgets(STDIN));
}
if (isTTY()) {
	system('stty -echo');
}
echo 'New password: ';
$newpass=trim(fgets(STDIN));
if (isTTY()) {
	echo "\n";
}
echo 'Repeate password: ';
$repeat=trim(fgets(STDIN));
if (isTTY()) {
	echo "\n";
}
if (isTTY()) {
	system('stty echo');
}
if ($newpass != $repeat) {
	echo 'Passwords do not match.'."\n";
	exit;
}
$user=user::select_by_email($email)->fetch();
if (!$user) {
	echo 'User not found.'."\n";
	exit;
}
$user->setPassword($newpass);
$user->write();
echo 'Password changed.'."\n";
?>
