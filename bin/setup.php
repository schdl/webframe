#!/usr/bin/env php
<?php
namespace WebFrame;
require_once(__DIR__.'/../Core.php');
setConf('debug', true);
// Explicitly connect without the USE statement
connectToDB(false);
$opts=getopt('R');
if (isset($opts['R'])) {
	query_db('DROP DATABASE IF EXISTS `'.getConf('dbname').'`');
}
query_db('CREATE DATABASE IF NOT EXISTS `'.getConf('dbname').'`'); // We can add charset and collate here if we want
query_db('USE `'.getConf('dbname').'`');
foreach (\DB\Table::allTables() as $table) {
	if (isset($opts['R'])) {
		$table->drop();
	}
	$table->create();
}
$user=\DB\Table::getClass('users');
$user=new $user;
$cols=$user->table()->columns;
// TODO fill values based on the columns defined for the object, don't hardcode
do {
	if ($user->email) {
		echo 'Invalid entry: '.$user->email."\n";
	}
	echo 'Admin email address: ';
	$user->email=trim(fgets(STDIN));
	if (!isTTY()) {
		echo "\n";
	}
} while (!EmailValidator::create()->validate($user->email));
echo 'Admin name: ';
$user->name(rtrim(fgets(STDIN)));
if (isTTY()) {
	system('stty -echo');
} else {
	echo "\n";
}
do {
	if (isset($pass)) {
		echo "Entered passwords did not match.  Try again.\n";
	}
	echo 'Admin password: ';
	$pass=trim(fgets(STDIN));
	echo "\nRepeat password: ";
	$passconfirm=trim(fgets(STDIN));
	echo "\n";
} while (!$pass || $pass != $passconfirm);
if (isTTY()) {
	system('stty echo');
}
if (substr($pass, 0, 5) == 'hash:') {
	$user->passhash=substr($pass, 5);
} else {
	$user->setPassword($pass);
}
unset($pass);
$user->flags='a'; // Admin
foreach ($cols as $col => $data) {
	if (!isset($user->$col) && !(isset($data->null) && $data->null) && !$data->auto_increment) {
		echo 'Admin '.$col.': ';
		$user->$col=trim(fgets(STDIN));
	}
}
$user->write();
foreach (glob(__DIR__.'/*_setup.php') as $file) {
	require($file);
}
?>
