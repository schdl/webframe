<?php
namespace WebFrame;
abstract class AbstractValidator {
	protected $error;
	abstract function validate($data);
	public function getError() {
		return $this->error;
	}
	public static function create() {
		$r=new \ReflectionClass(get_called_class());
		return $r->newInstanceArgs(func_get_args());
	}
	public function describe() {
		return $this->error;
	}
}
?>
