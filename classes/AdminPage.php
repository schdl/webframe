<?php
namespace WebFrame;
abstract class AdminPage extends StandardPage {
	function init() {
		if (user() === null) {
			return 'login';
		} elseif (!user()->has_flag('a')) {
			return '403';
		}
		return $this->_init();
	}
	function _init() {}
}
?>
