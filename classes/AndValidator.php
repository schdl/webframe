<?php
namespace WebFrame;
class AndValidator extends AbstractValidator {
	protected $validators;
	function __construct() {
		if (is_array($this->validators=func_get_arg(0))) {
			$this->validators=func_get_arg(0);
		} else {
			$this->validators=func_get_args();
		}
	}
	function add($validator) {
		$this->validators[]=$validator;
	}
	function validate($data) {
		foreach ($this->validators as $v) {
			if (($r=$v->validate($data)) !== true) {
				$this->error=$v->getError();
				return false;
			}
		}
		return true;
	}
	function describe() {
		$d='must meet the following conditions:<ul>';
		foreach ($this->validators as $validator) {
			$d.='<li>'.$validator->describe().'</li>';
		}
		$d.='</ul>';
		return $d;
	}
}
?>
