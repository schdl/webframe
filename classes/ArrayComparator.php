<?php
namespace WebFrame;
class ArrayComparator {
	protected $columns;
	function __construct() {
		if (func_num_args() == 1 && is_array(func_get_arg(0))) {
			$this->columns=func_get_arg(0);
		} else {
			$this->columns=func_get_args();
		}
	}
	function __invoke($a, $b) {
		foreach ($this->columns as $col) {
			if (isset($a[$col])) {
				if (!isset($b[$col])) {
					return 1;
				}
			} elseif (isset($b[$col])) {
				return -1;
			} else {
				continue;
			}
			if ($a[$col] > $b[$col]) {
				return 1;
			} elseif ($a[$col] < $b[$col]) {
				return -1;
			}
		}
		return 0;
	}
}
?>
