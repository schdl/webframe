<?php
namespace WebFrame;
class ArrayValidator extends AbstractValidator {
	protected $validators, $optional, $fail_on_extra, $purpose;
	public function __construct(array $validators=null, $optional=null, $fail_on_extra=false) {
		if (isset($validators)) {
			$this->validators=$validators;
		} else {
			$this->validators=array();
		}
		if (isset($optional)) {
			if (!is_array($optional)) {
				throw new \RuntimeException('$optional must be array');
			}
			$this->optional=array_flip($optional);
		} else {
			$this->optional=array();
		}
		$this->fail_on_extra=$fail_on_extra;
	}
	public function add($name, $validator, $optional=false, $purpose=null) {
		$this->validators[$name]=$validator;
		$this->optional[$name]=$optional;
		$this->purpose[$name]=$purpose;
	}
	public function validate($data) {
		if (is_object($data)) {
			$data=(array)$data;
		}
		$errors=array();
		foreach ($this->validators as $name => $validator) {
			if (!array_key_exists($name, $data)) {
				if (!(isset($this->optional[$name]) && $this->optional[$name])) {
					$errors[]=$name.' is required';
				}
				continue;
			}
			if (!$validator->validate($data[$name])) {
				$errors[]=$name.': '.$validator->getError();
			}
		}
		if ($this->fail_on_extra) {
			$diff=array_diff_key($data, $this->validators);
			if ($diff) {
				$errors[]='extraneous entries: '.implode(', ', array_keys($diff));
			}
		}
		if ($errors) {
			$this->error=implode('; ', $errors);
			return false;
		} else {
			return true;
		}
	}
	function describe() {
		$d='must be a map with the following keys (';
		if ($this->fail_on_extra) {
			$d.='extra values prohibited';
		} else {
			$d.='extra values allowed';
		}
		$d.='):<dl>';
		foreach ($this->validators as $name => $validator) {
			$d.='<dt><i>'.htmlize($name).'</i>';
			if (isset($this->optional[$name]) && $this->optional[$name]) {
				$d.=' (optional)';
			}
			if (isset($this->purpose[$name]) && $this->purpose[$name]) {
				$d.=' - '.$this->purpose[$name];
			}
			$d.='</dt><dd>'.$validator->describe().'</dd>';
		}
		$d.='</dl>';
		return $d;
	}
}
?>
