<?php
namespace WebFrame;
class CallbackValidator extends AbstractValidator {
	protected $callback, $args;
	public $descr='(description missing)';
	function __construct($callback, $args=array()) {
		$this->callback=$callback;
		if (!is_array($args)) {
			$args=array($args);
		}
		$this->args=$args;
	}
	function validate($data, $args=array()) {
		$result=call_user_func_array($this->callback, array_merge(array($data), $this->args, $args));
		if (is_string($result)) {
			$this->error=$result;
			return false;
		} elseif ($result) {
			return true;
		} else {
			$this->error='is not valid';
			return false;
		}
	}
	function describe() {
		return $this->descr;
	}
}
?>
