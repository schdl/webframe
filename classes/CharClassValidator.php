<?php
namespace WebFrame;
class CharClassValidator extends RegexpValidator {
	function __construct($class, $error_msg) {
		parent::__construct('/^['.$class.']*$/', $error_msg);
	}
}
?>
