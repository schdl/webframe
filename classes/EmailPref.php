<?php
namespace WebFrame;
abstract class EmailPref {
	abstract function addToForm($form);
	abstract function update($data);
	protected static function WebFrame($rc) {
		if ($rc->isInstantiable()) {
			registerClass('email_prefs', ['name' => get_called_class()], true);
		}
	}
}
