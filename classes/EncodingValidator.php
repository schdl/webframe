<?php
namespace WebFrame;
class EncodingValidator extends AbstractValidator {
	protected $encoding;
	function __construct($encoding) {
		$this->encoding=$encoding;
		$this->error='must be a valid '.$encoding.'-encoded string';
	}
	function validate($data) {
		return is_string($data) && mb_check_encoding($data, $this->encoding);
	}
}
?>
