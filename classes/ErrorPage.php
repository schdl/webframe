<?php
namespace WebFrame;
abstract class ErrorPage extends StandardPage {
	protected static $code, $msg, $type, $msg_title;
	protected $body='messagebox', $bodyData=array();
	function init() {
		if (!headers_sent()) {
			header('HTTP/1.1 '.static::$code.' '.static::$msg, true, static::$code);
		}
		$this->bodyData['type']=static::$type;
		$this->bodyData['title']=static::$msg_title?:$this->title;
		$this->bodyData['msg']=$this->getMessage();
	}
	function canonicalURL() {
		return null;
	}
	abstract function getMessage();
}
?>
