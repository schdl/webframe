<?php
namespace WebFrame;
abstract class FilterValidator extends AbstractValidator {
	protected $opts=null;
	function validate($data) {
		return filter_var($data, static::$filter, $this->opts) !== false;
	}
}
?>
