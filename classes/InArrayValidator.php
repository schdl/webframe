<?php
namespace WebFrame;
class InArrayValidator extends KeyInArrayValidator {
	function __construct() {
		if (func_num_args() == 1 && is_array(func_get_arg(0))) {
			parent::__construct(array_flip(func_get_arg(0)));
		} else {
			parent::__construct(array_flip(func_get_args()));
		}
	}
}
?>
