<?php
namespace WebFrame;
class IntValidator extends FilterValidator {
	protected static $filter=\FILTER_VALIDATE_INT, $mustbe='an integer';
	protected $min, $max;
	function __construct($min=null, $max=null) {
		$this->min=$min;
		$this->max=$max;
	}
	function validate($data) {
		if (parent::validate($data)) {
			if (isset($this->min) && $data < $this->min) {
				$this->error='must be at least '.$this->min;
				return false;
			} elseif (isset($this->max) && $data > $this->max) {
				$this->error='must be no more than '.$this->max;
				return false;
			} else {
				return true;
			}
		} else {
			$this->error='must be '.static::$mustbe;
			return false;
		}
	}
	function describe() {
		$d='must be '.static::$mustbe;
		if (isset($this->min)) {
			if (isset($this->max)) {
				$d.=' in the closed interval ['.$this->min.', '.$this->max.']';
			} else {
				$d.=' no less than '.$this->min;
			}
		} elseif (isset($this->max)) {
			$d.=' no greater than '.$this->max;
		}
		return $d;
	}
}
?>
