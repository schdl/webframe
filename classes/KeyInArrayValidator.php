<?php
namespace WebFrame;
class KeyInArrayValidator extends AbstractValidator {
	protected $values;
	function __construct() {
		if (func_num_args() == 1 && is_array(func_get_arg(0))) {
			$this->values=func_get_arg(0);
		} else {
			$this->values=array_flip(func_get_args());
		}
		if (count($this->values) == 1) {
			$this->error='must be '.implode('', array_keys($this->values));
		} else {
			$this->error='must be one of '.implode(', ', array_keys($this->values));
		}
	}
	function validate($data) {
		if (array_key_exists($data, $this->values)) {
			return true;
		} else {
			return false;
		}
	}
}
?>
