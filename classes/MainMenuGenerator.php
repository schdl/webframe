<?php
namespace WebFrame;
class MainMenuGenerator extends MenuGenerator {
	function getMenuItems() {
		$menu=array();
		if (user() !== null) {
			$path=getConf('http_path')?getConf('http_path'):'/';
			$user=array('title' => htmlize(user()->name()), 'precedence' => 75);
			if (getConf('allow_su') && isset($_COOKIE[getConf('cookiename').'_su']) && session()->get_user()->has_flag('a')) {
				$user['submenu']['end_su']=array('onclick' => 'document.cookie=\''.getConf('cookiename').'_su=; expires=Fri, 27 Jul 2001 02:47:11 UTC; path='.$path.'\'; document.location.href=\''.admin__user::url(array('user' => user()->id)).'\'', 'title' => '<img src="'.static_content::url('file=images/su.png').'" alt="Back" />', 'precedence' => 0);
			}
			if (user()->has_flag('a')) {
				$cn=getConf('cookiename').'_debug';
				if (!getConf('debug') || isset($_COOKIE[getConf('cookiename').'_debug'])) {
					if (getConf('debug')) {
						$user['submenu']['no_debug']=array('onclick' => 'document.cookie=\''.$cn.'=0; expires=Fri, 27 Jul 2001 02:47:11 UTC; path='.$path.'\'; document.location=document.location.href', 'title' => 'Disable Debugging');
					} else {
						$user['submenu']['debug']=array('onclick' => 'document.cookie=\''.$cn.'=1; path='.$path.'\'; document.location.reload()', 'title' => 'Enable Debugging');
					}
				}
				$user['submenu']['admin']=array('title' => 'Admin', 'href' => admin::url());
			}
			$user['submenu']['prefs']=array('href' => users__preferences::url(), 'title' => 'Preferences');
			if (getConf('invite') && (user()->has_flag('a') || getConf('invite') !== 'admin')) {
				$user['submenu']['invite']=array('href' => invite::url(), 'title' => 'Invite');
			}
			$user['submenu']['logout']=array('href' => logout::url(strlen(request())?array('go' => request()):null), 'title' => 'Logout');
			$menu['user']=$user;
		} else {
			if (page()->page() == 'login' || page()->page() == 'logout') {
				$menu['login']=array('href' => login::url(), 'title' => 'Login');
			} else {
				$menu['login']=array('href' => login::url(strlen(request())?array('go' => request()):null), 'title' => 'Login');
			}
			if (getConf('registration')) {
				$menu['login']['submenu']['registration']=array('href' => create_account::url(), 'title' => 'Create Account');
			}
		}
		return $menu;
	}
}
?>
