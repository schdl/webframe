<?php
namespace WebFrame;
abstract class MenuGenerator extends Findable {
	abstract function getMenuItems();
	protected static function WebFrame($rc) {
		if (!$rc->isInstantiable()) {
			return;
		}
		registerClass('menus');
	}
}
?>
