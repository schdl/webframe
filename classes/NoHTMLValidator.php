<?php
namespace WebFrame;
class NoHTMLValidator extends AbstractValidator {
	protected $allowtags;
	function __construct($allowtags=null) {
		$this->allowtags=$allowtags;
		$this->error='must be a string that does not contain HTML tags'.($this->allowtags?' except '.htmlize($this->allowtags):'');
	}
	function validate($data) {
		if (!is_string($data)) {
			return false;
		}
		if (strip_tags($data, $this->allowtags) === $data) {
			return true;
		} else {
			return false;
		}
	}
}
?>
