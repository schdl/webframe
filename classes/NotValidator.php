<?php
namespace WebFrame;
class NotValidator extends AbstractValidator {
	protected $validator, $error_msg;
	function __construct($validator, $error_msg=null) {
		$this->validator=$validator;
		$this->error_msg=isset($error_msg)?$error_msg:'is not valid';
	}
	function validate($data) {
		if ($this->validator->validate($data)) {
			$this->error=$this->error_msg;
			return false;
		} else {
			return true;
		}
	}
	function describe() {
		return 'must not meet the following condition:<ul><li>'.$this->validator->describe().'</li></ul>';
	}
}
?>
