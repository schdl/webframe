<?php
namespace WebFrame;
class NullableValidator extends AbstractValidator {
	protected $validator;
	function __construct($validator) {
		$this->validator=$validator;
	}
	function validate($data) {
		if ($data === null || $this->validator->validate($data)) {
			return true;
		} else {
			$this->error=$this->validator->getError();
			return false;
		}
	}
	function describe() {
		return '(may be null) '.$this->validator->describe();
	}
}
?>
