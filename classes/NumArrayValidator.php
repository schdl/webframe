<?php
namespace WebFrame;
class NumArrayValidator extends AbstractValidator {
	protected $validator;
	function __construct($validator) {
		$this->validator=$validator;
	}
	public function validate($data) {
		if (!is_array($data)) {
			$this->error='Must be array';
			return false;
		}
		foreach ($data as $key => $val) {
			if (!$this->validator->validate($val)) {
				$this->error=$key.': '.$this->validator->getError();
				return false;
			}
		}
		return true;
	}
	function describe() {
		return 'must be a list where all values meet the condition:<ul><li>'.$this->validator->describe().'</li></ul>';
	}
}
?>
