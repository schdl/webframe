<?php
namespace WebFrame;
class OrValidator extends AndValidator {
	function validate($data) {
		$errors=array();
		foreach ($this->validators as $v) {
			if (($r=$v->validate($data)) === true) {
				return true;
			} else {
				$errors[]=$v->getError();
			}
		}
		$this->error=implode('; ', $errors);
		return false;
	}
	function describe() {
		$d='must meet at least one of the following conditions:<ul>';
		foreach ($this->valiadtors as $validator) {
			$d.='<li>'.$validator->describe().'</li>';
		}
		$d.='</ul>';
		return $d;
	}
}
?>
