<?php
namespace WebFrame;
abstract class Page extends Findable {
	public $args=array();
	public $title=array();
	public $description;
	public static $routing=array();
	public static function isAllowed() {
		return true;
	}
	public function init() {}
	public function header() {}
	public function body() {}
	public function footer() {}
	public function run() {
		if ($this->isMainPage()) {
			$this->init();
			$this->header();
			$this->body();
			$this->footer();
		} else {
			$this->init();
			$this->body();
		}
	}
	public function isMainPage() {
		return $this === Core::$page;
	}
	public static function module() {
		$class=get_called_class();
		$pos=strrpos($class, '\\');
		return substr($class, 0, $pos);
	}
	public static function page() {
		$class=get_called_class();
		$pos=strrpos($class, '\\');
		return substr($class, $pos+1);
	}
	public static function url($args=null) {
		if ($args === null) {
			$args=array();
		} elseif (is_string($args)) {
			$parts=explode(';', $args);
			$args=array();
			foreach ($parts as $part) {
				list($name, $val)=explode('=', $part, 2);
				$args[$name]=$val;
			}
		}
		if (array_key_exists('_get', $args)) {
			$get=$args['_get'];
			unset($args['_get']);
			if (is_array($get)) {
				$i=0;
				$tmp='';
				foreach ($get as $name => $val) {
					if ($i++ == 0) {
						$tmp.='?';
					} else {
						$tmp.='&';
					}
					$tmp.=$name;
					if (isset($val)) {
						$tmp.='='.$val;
					}
				}
				$get=$tmp;
			} elseif (isset($get)) {
				$get='?'.$get;
			} else {
				$get='';
			}
		} else {
			$get='';
		}
		foreach (static::$routing as $pattern => $vars) {
			//debug(__METHOD__, 'Pattern: '.$pattern);
			$varnames=array();
			if (isset($vars)) {
				unset($vars['precedence']);
			} else {
				$vars=array();
			}
			foreach ($vars as $name => $val) {
				// Positional parameter
				if (is_int($name) && isset($args[$val])) {
					//debug(__METHOD__, 'Parameter '.count($varnames).': '.$val);
					$varnames[]=$val;
				// Fixed-value parameter
				} elseif (isset($args[$name]) && $args[$name] == $val) {
					//debug(__METHOD__, 'Fixed parameter "'.$name.'"="'.$var.'"');
					// The same value was specified
					$varnames[]=$name;
				} else {
					//debug(__METHOD__, 'Fixed parameter "'.$name.'"="'.$val.'" not matched');
					// The same value was not specified - no match here
					continue 2;
				}
			}
			// If the parameter lists are different, no match found
			if (count($args) != count($varnames)) {
				//debug(__METHOD__, 'Expected '.implode(', ', $varnames).' but got '.implode(', ', array_keys($args)));
				continue;
			}
			/****** And here comes the awesome URL-constructor part: *****\
			 * TODO:
			 * -Handle character classes []
			 * -Handle {n}, {m,n}, {,n}, {m,}, ?, *, and + properly
			 * -This will require more sophisticated handling so it would
			 *  probably be a good idea to create a generic reverse-regex
			 *  function and use helper functions there
			\*************************************************************/
			$url='';
			$bs=false;
			$mode=null;
			$param=0;
			for ($i=0; $i<strlen($pattern); $i++) {
				$char=$pattern[$i];
				switch($mode) {
					case '(':
						switch($char) {
							case '?':
								$mode='?';
								break;
							case ')':
								// Backtrack and handle this as a completed parenthesized subexpression
								$i--;
								$mode=')';
								break;
							case '\\':
								$bs=true;
								break;
							default:
								$mode=')';
								break;
						}
						break;
					case ')':
						if ($bs) {
							$bs=false;
						} else {
							switch($char) {
								case '\\':
									$bs=true;
									break;
								case ')':
									$mode=null;
									if ($param >= count($varnames)) {
										debug(__METHOD__, 'Found more parenthesized sub-expressions than we have arguments!');
									} else {
										$url.=$args[$varnames[$param++]];
									}
									break;
								default:
									// Do nothing
							}
						}
						break;
					case '|)':
						if ($bs) {
							$bs=false;
						} else {
							switch($char) {
								case ')':
									$mode=null;
									break;
								default:
									// Do nothing
							}
						}
						break;
					case '?':
						switch($char) {
							case ':':
								$mode='|';
								break;
							case ')':
								debug(__METHOD__, 'Illegal substring (?) in pattern '.$pattern);
								$mode=null;
								break;
							default:
								debug(__METHOD__, 'Unknown command (?'.$char.' in pattern '.$pattern);
								$mode=null;
						}
						break;
					case '|':
						if ($bs) {
							$url.=$char;
							$bs=false;
						} else {
							switch($char) {
								case '|':
									$mode='|)';
									break;
								case ')':
									$mode=null;
									break;
								default:
									$url.=$char;
							}
						}
						break;
					default:
						if ($bs) {
							$url.=$char;
							$bs=false;
						} else {
							switch($char) {
								case '|':
									break 3;
								case '(':
									$mode='(';
									break;
								case ')':
									debug(__METHOD__, 'Unexpected ) in pattern '.$pattern);
									break;
								case '\\':
									$bs=true;
									break;
								case '?':
									// We already inserted it
									break;
								case '[':
								case '{':
									debug(__METHOD__, 'Handling of '.$char.' not yet implemented - treating as literal '.$char);
								case '.':
									debug(__METHOD__, 'Unexepcted . in pattern '.$pattern.' - adding a literal .');
								default:
									$url.=$char;
							}
						}
				}
			}
			if (preg_match('{^'.$pattern.'$}', $url)) {
				//debug(get_called_class().'::'.__FUNCTION__, 'Constructed URL '.$url.' and it matched!');
				return url($url.$get);
			} else {
				//debug(get_called_class().'::'.__FUNCTION__, 'Constructed URL '.$url.' but it did not match');
			}
		}
		throw new \InvalidArgumentException('Could not construct a URL with arguments '.var_export($args, true));
	}
	public static function link($text=null, $args=null, $extra='') {
		$url=static::url($args);
		if ($text === null) {
			$text=htmlize($url);
		}
		return '<a href="'.str_replace('"', '\\"', $url).'"'.(strlen($extra)?' '.$extra:'').'>'.$text.'</a>';
	}
	public function canonicalURL() {
		return static::url($this->args);
	}
	public static function sitemapURLs() {}
	public static function mtime() {
		$rc=new \ReflectionClass(get_called_class());
		return new \DateTime('@'.filemtime($rc->getFileName()));
	}
	protected static function defaultSitemapURL() {
		return SitemapURL::create(static::url())->lastmod(static::mtime());
	}
	protected static function WebFrame($rc) {
		if (!$rc->isInstantiable()) {
			return;
		}
		registerClass('pages', isset(static::$precedence)?['precedence' => static::$precedence]:[]);
		foreach (static::$routing as $pattern => $args) {
			$route=array('pattern' => $pattern);
			if (isset($args)) {
				if (isset($args['precedence'])) {
					$route['precedence']=$args['precedence'];
					unset($args['precedence']);
				} elseif (isset(static::$precedence)) {
					$route['precedence']=static::$precedence;
				}
				if (count($args)) {
					$route['args']=$args;
				}
			} elseif (isset(static::$precedence)) {
				$route['precedence']=static::$precedence;
			}
			registerRoute($route);
		}
	}
}
?>
