<?php
namespace WebFrame;
class RegexpValidator extends AbstractValidator {
	protected static $filter=\FILTER_VALIDATE_REGEXP;
	protected $regexp, $error_msg;
	function __construct($regexp, $error_msg=null) {
		$this->regexp=$regexp;
		if (isset($error_msg)) {
			$this->error=$error_msg;
		} else {
			$this->error='must match the regular expression '.htmlize($regexp);
		}
	}
	function validate($data) {
		if (preg_match($this->regexp, $data)) {
			return true;
		} else {
			return false;
		}
	}
}
?>
