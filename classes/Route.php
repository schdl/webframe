<?php
namespace WebFrame;
class Route {
	function __construct($data) {
		foreach ($data as $key => $value) {
			$this->$key=$value;
		}
	}
	function match($request, $method, $get, $post) {
		if (isset($this->method)) {
			$match=is_array($this->method)?array_change_key_case(array_flip($this->method)):[strtolower($this->method) => ''];
			if (!isset($match[strtolower($method)])) {
				return false;
			}
		}
		$args=[];
		if (isset($this->pattern)) {
			if (!preg_match('{^'.$this->pattern.'$}', $request, $matches)) {
				return false;
			}
			$args=self::map_args($matches, isset($this->args)?$this->args:[]);
		}
		$obj=new $this->class;
		$obj->args=$args;
		return $obj;
	}
	private static function map_args($matches, $arg_def) {
		$args=[];
		$i=0;
		foreach ($arg_def as $name => $value) {
			if ($name === $i) {
				debug(__FUNCTION__, $name.' is numeric - using '.$value.' instead');
				$name=$value;
				if ($i >= count($matches)) {
					debug(__FUNCTION__, 'Trying to set argument "'.$name.'" but ran out of matches (page='.$this->class.')');
				} else {
					$args[$name]=$matches[++$i];
				}
			} else {
				debug(__FUNCTION__, $name.' is not numeric, setting it to '.$value);
				$args[$name]=$value;
			}
		}
		return $args;
	}
}
?>
