<?php
namespace WebFrame;
class SitemapURL {
	public $url, $lastmod, $changefreq, $priority;
	public static function create($url) {
		return new static($url);
	}
	function __construct($url) {
		$this->url=$url;
	}
	function lastmod($time) {
		$this->lastmod=$time;
		return $this;
	}
	function changefreq($freq) {
		$this->changefreq=$freq;
		return $this;
	}
	function priority($priority) {
		$this->priority=$priority;
		return $this;
	}
	function toXML() {
		$r="<url>\n\t<loc>".htmlize($this->url)."</loc>\n";
		if (isset($this->lastmod)) {
			$r.="\t<lastmod>";
			if (is_a($this->lastmod, 'DateTime')) {
				$r.=$this->lastmod->format('c');
			} else {
				$r.=$this->lastmod;
			}
			$r.="</lastmod>\n";
		}
		if (isset($this->changefreq)) {
			$r.="\t<changefreq>$this->changefreq</changefreq>\n";
		}
		if (isset($this->priority)) {
			$r.="\t<priority>$this->priority</priority>\n";
		}
		$r.="</url>\n";
		return $r;
	}
}
?>
