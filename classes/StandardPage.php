<?php
namespace WebFrame;
abstract class StandardPage extends Page {
	protected $header='header', $body, $footer='footer';
	protected $headerData, $bodyData=array(), $footerData;
	function header() {
		if (isset($this->header)) {
			user();
			runTemplate($this->header, $this->headerData?:self::standardData());
		}
	}
	function body() {
		runTemplate(isset($this->body)?$this->body:str_replace('__', '/', $this->page()), $this->bodyData?:self::standardData());
	}
	function footer() {
		if (isset($this->footer)) {
			runTemplate($this->footer, $this->footerData?:self::standardData());
		}
	}
	public static function standardData() {
		$data=array();
		if ($page=page()) {
			$data['title']=$page->title;
			if (!is_array($data['title'])) {
				$data['title']=array($data['title']);
			}
			$data['description']=$page->description;
			$data['canonical_url']=$page->canonicalURL();
		}
		$scripts=array();
		foreach (Core::$scripts as $script) {
			if (strtolower(substr($script, 0, 4)) === 'http') {
				$scripts[]=$script;
			} else {
				$scripts[]=static_content::url('file=js/'.$script.'.js');
			}
		}
		$data['scripts']=$scripts;
		return $data;
	}
}
?>
