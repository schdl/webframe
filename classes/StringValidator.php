<?php
namespace WebFrame;
class StringValidator extends AbstractValidator {
	protected $min, $max;
	function __construct($min=null, $max=null) {
		$this->min=$min;
		$this->max=$max;
	}
	function validate($data) {
		if (!is_string($data)) {
			$this->error='must be string';
			return false;
		} elseif (isset($this->min) && strlen($data) < $this->min) {
			$this->error='must be at least '.$this->min.' characters';
			return false;
		} elseif (isset($this->max) && strlen($data) > $this->max) {
			$this->error='must be at most '.$this->max.' characters';
			return false;
		} else {
			return true;
		}
	}
	function describe() {
		$d='must be a string';
		if (isset($this->min)) {
			if (isset($this->max)) {
				$d.=' that is '.$this->min.'-'.$this->max.' bytes long';
			} else {
				$d.=' that is at least '.$this->min.' bytes long';
			}
		} elseif (isset($this->max)) {
			$d.=' that is at most '.$this->max.' bytes long';
		}
		return $d;
	}
}
?>
