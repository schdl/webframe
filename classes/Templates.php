<?php
namespace WebFrame;
abstract class Templates {
	private static function WebFrame() {
		registerRegistrar('CacheTemplates');
	}
	public static function CacheTemplates() {
		foreach (modules() as $module) {
			$dir=templateDir($module);
			if (!isset($dir)) {
				continue;
			}
			$it=new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dir, \FilesystemIterator::FOLLOW_SYMLINKS));
			for (; $it->valid(); $it->next()) {
				if ($it->isDot() || $it->isDir() || !$it->isReadable() || $it->getExtension() != 'php') {
					continue;
				}
				$path=$it->getSubPathName();
				$path=substr($path, 0, strlen($path)-4);
				if (!isset(Core::$cache['templates'][$path]) || !in_array($module, Core::$cache['templates'][$path])) {
					Core::$cache['templates'][$path][]=$module;
				}
			}
		}
	}
}
?>
