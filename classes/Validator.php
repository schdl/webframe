<?php
namespace WebFrame;
interface Validator {
	public function validate($data);
	public function getError();
	public static function create();
	public function describe();
}
?>
