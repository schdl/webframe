<?php
namespace WebFrame;
class checkbox_array extends form_element {
	protected $array;
	function __construct($htmlname, $label, $array, $delim=null) {
		parent::__construct($htmlname, $label);
		$this->array=$array;
		$this->delim=$delim;
	}
	public function output($val=false, $rw=true) {
		$this->set_val($val);
		if (strlen($this->label))
			echo "<b>$this->label:</b><br/>\n";
		$i=0;
		foreach ($this->array as $value => $label) {
			$label=htmlize($label);
			if ($rw)
				echo "\t<input type=\"checkbox\" id=\"$this->htmlname-$i\" name=\"$this->htmlname[$i]\"".($this->val_has($value)?' checked="checked"':'')." /><label for=\"$this->htmlname-$i\">$label</label> \n";
			elseif ($this->val_has($value))
				echo "$label<br/>\n";
			$i++;
		}
	}
	public function process() {
		$val=array();
		if (isset($_REQUEST[$this->htmlname])) {
			$vals=array_keys($this->array);
			foreach ($_REQUEST[$this->htmlname] as $i => $null) {
				$val[]=$vals[$i];
			}
		}
		return isset($this->delim)?implode($this->delim, $val):$val;
	}
	public function verify($val) {
		if ($val === false) return false;
		if (count($val) == 0) return true;
		if (isset($this->delim)) {
			$val=explode($this->delim, $val);
		}
		foreach ($val as $i => $value) {
			if (isset($this->array[$value])) {
				unset($val[$i]);
			}
		}
		return count($val) == 0;
	}
	private $vals;
	protected function set_val($val) {
		$this->vals=isset($this->delim)?explode($this->delim, $val):$val;
	}
	protected function val_has($needle) {
		return is_array($this->vals) && in_array($needle, $this->vals);
	}
}
?>
