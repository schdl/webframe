<?php
namespace WebFrame;
class email_verification extends \DB\Row {
	public static function create($type, $interval, $email, $user=null) {
		if (is_object($user)) {
			$user=$user->id;
		}
		$ev=new self;
		$ev->type=$type;
		$ev->email=$email;
		$ev->user=$user;
		$expiration=now()->add($interval);
		$ev->expiration=$expiration->format('Y-m-d H:i:s');
		$tries=0;
		while ($ev->key === null) {
			$ev->key=randstring(30);
			try {
				$ev->write();
			} catch (\PDOException $e) {
				$ev->key=null;
			}
			$tries++;
			if ($tries >= 10) {
				throw new \RuntimeException('Failed to create unique email_verification key after 10 tries');
			}
		}
		return $ev;
	}
	public static function validate($type, $key, $user=null) {
		if (isset($user)) {
			$ev=static::wrapped_query('SELECT * FROM `email_verifications` WHERE `type` = ? AND `key` = ? AND `user` = ? AND `expiration` > ?', array($type, $key, $user, now()->format('Y-m-d H:i:s')))->fetch();
		} else {
			$ev=static::wrapped_query('SELECT * FROM `email_verifications` WHERE `type` = ? AND `key` = ? AND `expiration` > ?', array($type, $key, now()->format('Y-m-d H:i:s')))->fetch();
		}
		if ($ev) {
			return $ev;
		} else {
			return false;
		}
	}
}
?>
