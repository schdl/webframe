<?php
namespace WebFrame;
// TODO make an option to use actual values in the form for selects, checkbox arrays, and radios instead of indices
class form {
	protected $action, $method, $enctype, $elements=array();
	function __construct($action=null, $method='post', $enctype=null) {
		global $S;
		$this->action=$action;
		$this->method=$method;
		$this->enctype=$enctype;
	}
	public function output($vals=array(), $rw=true) {
		if ($rw)
			echo '<form'.(isset($this->action)?' action="'.$this->action.'"':'').' method="'.$this->method.'"'.($this->enctype?'enctype="'.$this->enctype.'"':'').'>';
		foreach ($this->elements as $name => &$el) {
			if (!$el->status)
				echo print_warning('Please complete this field:');
			$el->output(isset($vals[$name])?$vals[$name]:false, $rw);
		}
		if ($rw)
			echo '</form>';
	}
	public function process() {
		$vals=array();
		foreach ($this->elements as $name => &$el) {
			$vals[$name]=$el->process();
			$el->status=$vals[$name] !== false;
		}
		return $vals;
	}
	public function verify($vals) {
		foreach ($this->elements as $name => &$el) {
			if (!($el->status=$el->verify(isset($vals[$name])?$vals[$name]:false))) {
				return $el->status;
			}
		}
		return true;
	}
	public function text($text) {
		$this->elements[]=new form_text($text);
	}
	public function rw_text($text) {
		$this->elements[]=new form_rw_text($text);
	}
	public function ro_text($text) {
		$this->elements[]=new form_ro_text($text);
	}
	public function text_input($optname, $htmlname, $label, $size=null) {
		$this->elements[$optname]=new text_input($htmlname, $label, $size);
	}
	public function numeric_input($optname, $htmlname, $label, $size=null) {
		$this->elements[$optname]=new numeric_input($htmlname, $label, $size);
	}
	public function password($optname, $htmlname, $label) {
		$this->elements[$optname]=new form_password($htmlname, $label);
	}
	public function hidden($optname, $htmlname, $value) {
		$this->elements[$optname]=new form_hidden_input($htmlname, $value);
	}
	public function select($optname, $htmlname, $label, $options, $br=true) {
		$this->elements[$optname]=new select($htmlname, $label, $options);
		if ($br) {
			$this->text('<br/>');
		}
	}
	public function radio_array($optname, $htmlname, $label, $options) {
		$this->elements[$optname]=new radio_array($htmlname, $label, $options);
	}
	public function checkbox_array($optname, $htmlname, $label, $array, $delim=null) {
		$this->elements[$optname]=new checkbox_array($htmlname, $label, $array, $delim);
	}
	public function layered_checkbox_array($optname, $htmlname, $label, &$array, $delim=null, $metadata) {
		$this->elements[$optname]=new layered_checkbox_array($htmlname, $label, $array, $delim, $metadata);
	}
	public function submit($text=null) {
		$this->rw_text('<input type="submit" value="'.($text?htmlize($text):'Submit').'" />');
	}
}
?>
