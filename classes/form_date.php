<?php
namespace WebFrame;
class form_date extends form_element {
	private $month, $day, $year;
	function __construct($htmlname) {
		$this->month=new select($htmlname.'_month', 'Month', array(
			1 => 'January',
			2 => 'February',
			3 => 'March',
			4 => 'April',
			5 => 'May',
			6 => 'June',
			7 => 'July',
			8 => 'August',
			9 => 'September',
			10 => 'October',
			11 => 'November',
			12 => 'December'
		));
		$this->day=new numeric_input($htmlname.'_day', 'Day', 2);
		$this->year=new numeric_input($htmlname.'_year', 'Year', 4);
	}
	// TODO split value into parts using date
	function output($val=false, $rw=true) {
		if ($this->rw) {
			$this->month->output();
			$this->day->output();
			$this->year->output();
		} else {
		}
	}
	// TODO handle
	function process() {
		$month=$this->month->process();
		$day=$this->day->process();
		$year=$this->year->process();
	}
	// TODO verify
}
?>
