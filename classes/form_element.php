<?php
namespace WebFrame;
abstract class form_element {
	protected $htmlname, $label;
	public $status=true;
	function __construct($htmlname, $label) {
		$this->htmlname=htmlize($htmlname);
		$this->label=htmlize($label);
	}
	public function output($val=false, $rw=true) {
		echo "<b>$this->label:</b> ";
	}
	public function process() {
		return isset($_REQUEST[$this->htmlname])?$_REQUEST[$this->htmlname]:false;
	}
	public function verify($val) {
		return $val !== false;
	}
}
?>
