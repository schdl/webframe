<?php
namespace WebFrame;
class form_hidden_input extends form_element {
	private $value;
	function __construct($htmlname, $value) {
		$this->htmlname=$htmlname;
		$this->value=$value;
	}
	public function output($val=false, $rw=true) {
		if ($rw) {
			echo '<input type="hidden" name="'.$this->htmlname.'" value="'.htmlize($this->value).'" />';
		}
	}
}
?>
