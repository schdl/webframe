<?php
namespace WebFrame;
class form_text extends form_element {
	protected $text, $rw;
	function __construct($text, $rw=null) {
		$this->text=$text;
		$this->rw=$rw;
	}
	public function output($val=null, $rw=true) {
		if (!isset($this->rw) || $this->rw == $rw) echo $this->text;
	}
	public function process() {
		return null;
	}
	public function verify($val) {
		return true;
	}
}
?>
