<?php
namespace WebFrame;
class layered_checkbox_array extends checkbox_array {
	private $depth=0, $path_delims=array('', '/', '-');
	function __construct($htmlname, $label, &$array, $delim=null, $metadata) {
		parent::__construct($htmlname, $label, $array, $delim);
		$this->metadata=$metadata;
		for ($i=current($array); is_array($i); $i=current($i)) $this->depth++;
		global $S;
		if (!in_array('lca', Core::$scripts)) {
			addScript('lca');
		}
	}
	public function output($val=false, $rw=true) {
		$this->set_val($val);
		if ($this->label) {
			echo '<h4>'.htmlize($this->label).'</h4>';
		}
		if ($rw)
			$this->r_output($this->array);
		else
			$this->r_ro_output($this->array);
	}
	public function process() {
		$val=$this->r_process($this->array);
		return isset($this->delim)?implode($this->delim, $val):$val;
	}
	public function verify($val) {
		if ($val === false) return false;
		if (!is_array($val) && strlen($val) == 0) return true;
		if (isset($this->delim)) {
			$val=explode($this->delim, $val);
		}
		$r=$this->r_verify($val, $this->array);
		debug('lca', 'verify leftovers: '.implode(' ',$r));
		return count($r) == 0;
	}
	private function r_output(&$array, $depth=0, $path=null, $name=null) {
		static $uid=0, $ucid=0;
		$S['conf']=&$this->metadata[0];
		if ($depth == 0) {
			$search=$autosize=0;
			for ($i=1; $i<count($this->metadata); $i++) {
				$m=&$this->metadata[$i];
				if (isset($m['tag'])) {
					$autosize++;
				}
				if (isset($m['search'])) {
					$search++;
				}
			}
			if ($search) {
				if (!isset($S['conf']['id'])) {
					$S['conf']['id']=self::b36($uid++);
				}
				echo 'Filter: <input id="'.$S['conf']['id'].'-q" onkeyup="lca_search(this.value, document.getElementById(\''.$S['conf']['id'].'\'), 0, '.$this->depth.')" autocomplete="off" /> <a href="javascript:q=document.getElementById(\''.$S['conf']['id'].'-q\'); q.value=\'\'; q.onkeyup()">Clear</a> <a href="javascript:lca_show_checked(document.getElementById(\''.$S['conf']['id'].'\'), 0, '.$this->depth.'); undefined">Show checked</a><br/>'."\n";
			}
			echo '<div class="lca'.(isset($S['conf']['autosize']) && $S['conf']['autosize']?' autosize" style="font-size: '.pow(1.15, $autosize)*100.0.'%':'').'" id="'.$S['conf']['id'].'">'."\n";
			foreach ($array as $name => &$val) {
				$this->r_output($val, $depth+1, $name, $name);
				$uid++;
			}
			echo '<h3 style="display: none">No results</h3></div>';
			echo "<script type=\"text/javascript\">\n<!--\nif (lca_show_checked(document.getElementById('{$S['conf']['id']}'), 0, $this->depth) == 0) lca_search(document.getElementById('{$S['conf']['id']}-q').value, document.getElementById('{$S['conf']['id']}'), 0, $this->depth);\n-->\n</script>\n";
		} else {
			$meta=$this->metadata[$depth];
			if (isset($meta['tag'])) {
				echo '<'.$meta['tag'].' class="lcae'.(isset($meta['search'])?' lcas':'').(isset($meta['collapsed'])?' lca'.($meta['collapsed']?'c':'C'):'').(isset($meta['class'])?' '.$meta['class']:'').'" id="'.self::b36($uid).'"'.($depth > 1 && isset($this->metadata[$depth-1]['collapsed']) && $this->metadata[$depth-1]['collapsed'] && false?' style="display: none"':'').'>';
				if (isset($meta['collapsed']) && $depth < $this->depth) {
					echo '<a href="javascript:lcat(\''.self::b36($uid).'\')">&plusmn;</a>';
				}
			}
			if (isset($meta['checkbox'])) {
				$enc=self::b36($ucid++);
				echo '<input type="checkbox" id="-'.$enc.'" name="'.$this->htmlname.'['.$enc.']"'.($this->val_has($this->format_label($array, $meta['checkbox'], $path, $name))?' checked="checked"':'').' /><label for="-'.$enc.'">'.$this->format_label($array, $meta['label'], $path, $name).'</label>'."\n";
			} elseif (isset($meta['label'])) {
				echo '<span class="lcal">'.$this->format_label($array, $meta['label'], $path, $name)."</span>\n";
			}
			if ($depth < $this->depth) {
				foreach ($array as $name => &$val) {
					$uid++;
					$this->r_output($val, $depth+1, $path.$meta['delim'].$name, $name);
				}
			}
			if (isset($meta['tag'])) {
				echo '</'.$meta['tag'].">\n";
			}
		}
	}
	private function r_process(&$array, $depth=0, $path=null, $name=null) {
		static $ucid=0, $r;
		if ($depth == 0) {
			$r=array();
			foreach ($array as $name => &$val) {
				$this->r_process($val, $depth+1, $name, $name);
			}
			return $r;
		} else { 
			$meta=$this->metadata[$depth];
			if (isset($meta['checkbox'])) {
				if (isset($_REQUEST[$this->htmlname][self::b36($ucid)])) {
					$r[]=$this->format_label($array, $meta['checkbox'], $path, $name);
				}
				$ucid++;
			}
			if ($depth < $this->depth) {
				foreach ($array as $name => &$val)
					$this->r_process($val, $depth+1, $path.$meta['delim'].$name, $name);
			}
		}
	}
	private function r_verify(&$vals, &$array, $depth=0, $path=null, $name=null) {
		if ($depth == 0) {
			foreach($array as $name => &$val) {
				$this->r_verify($vals, $val, $depth+1, $name, $name);
			}
			return $vals;
		} else {
			$meta=$this->metadata[$depth];
			if (isset($meta['checkbox'])) {
				$label=$this->format_label($array, $meta['checkbox'], $path, $name);
				if (($i=array_search($label, $vals)) !== false) {
					unset($vals[$i]);
				}
			}
			if ($depth < $this->depth) {
				foreach ($array as $name => &$val)
					$this->r_verify($vals, $val, $depth+1, $path.$meta['delim'].$name, $name);
			}
			return $vals;
		}
	}
	private function r_ro_output(&$array, $depth=0, $path=null, $name=null) {
		if ($depth == 0) {
			foreach ($array as $name => &$val) {
				$this->r_ro_output($val, $depth+1, $name, $name);
			}
		} else {
			$meta=$this->metadata[$depth];
			if (isset($meta['checkbox'])) {
				$val=$this->format_label($array, $meta['checkbox'], $path, $name);
				if ($this->val_has($val)) {
					echo $this->format_label($array, $meta['label'], $path, $name)."<br/>\n";
				}
			}
			if ($depth < $this->depth) {
				foreach ($array as $name => &$val)
					$this->r_ro_output($val, $depth+1, $path.$meta['delim'].$name, $name);
			}
		}
	}
	private function format_label(&$array, $label='%p', $path, $name) {
		$arg=$array;
		$out=str_replace(array('%p', '%n'), array($path, $name), $label);
		if (strpos($label, '$')) {
			while (is_array(current($arg))) {
				$arg=current($arg);
			}
			$out=eval("extract(\$arg, EXTR_PREFIX_INVALID, 'var_');\n".(strpos($label, 'return')===0?$out:"return <<<_XQ1\n$out\n_XQ1").";\n");
		}
		return strpos($label, 'return')===0?$out:htmlize($out);
	}
	private static function b36($n) {
		return base_convert($n, 10, 36);
	}
}
?>
