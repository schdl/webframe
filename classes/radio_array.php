<?php
namespace WebFrame;
class radio_array extends select {
	public function output($val=false, $rw=true) {
		if (!$rw) return parent::output($val, $rw);
		echo "$this->label:<br/>\n";
		$i=0;
		foreach ($this->options as $value => $label) {
			echo "\t<input type=\"radio\" id=\"$this->htmlname-$i\" name=\"$this->htmlname\" value=\"".$i."\"".($value == $val?' checked="checked"':'')."\" /><label for=\"$this->htmlname-$i\">".htmlize($label)."</label><br/>\n";
			$i++;
		}
	}
}
?>
