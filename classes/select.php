<?php
namespace WebFrame;
class select extends form_element {
	private $options;
	function __construct($htmlname, $label, $options) {
		parent::__construct($htmlname, $label);
		$this->options=$options;
	}
	public function output($val=false, $rw=true) {
		parent::output($val, $rw);
		if ($rw) {
			echo '<select name="'.$this->htmlname.'">'."\n";
			$i=0;
		}
		foreach ($this->options as $value => $label) {
			if ($rw)
				echo "\t".'<option value="'.$i++.'"'.($value == $val?' selected="selected"':'').'>'.htmlize($label).'</option>'."\n";
			elseif ($value == $val)
				echo htmlize($label);
		}
		if ($rw)
			echo '</select>';
		echo "\n";
	}
	public function process() {
		$vals=array_keys($this->options);
		if (isset($_REQUEST[$this->htmlname]) && is_numeric($_REQUEST[$this->htmlname]) && isset($vals[$_REQUEST[$this->htmlname]])) {
			return $vals[$_REQUEST[$this->htmlname]];
		} else return false;
	}
	public function verify($val) {
		return isset($this->options[$val]);
	}
}
?>
