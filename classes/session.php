<?php
namespace WebFrame;
class session extends \DB\Row {
	// Creates a new session for the current user (user()) with a unique id, sends a cookie to the user and returns true for success, false for failure
	public static function create() {
		$id=null;
		while (!isset($id)) {
			$id=randstring(30);
			$s=self::select_by_id($id)->fetch();
			if ($s !== false) {
				$id=null;
			}
		}
		$s=new session();
		$s->id=$id;
		$s->user=user()->id;
		$s->atime=time();
		$s->expire=getConf('sessionlength');
		if (setcookie(null, $s->id, time()+getConf('sessionlength'))) {
			$s->write();
			Core::$session=$s;
			return true;
		} else {
			return false;
		}
	}
}
?>
