<?php
namespace WebFrame;
class text_input extends form_element {
	protected $size;
	function __construct($htmlname, $label, $size=null) {
		parent::__construct($htmlname, $label);
		$this->size=$size;
	}
	public function output($val=false, $rw=true) {
		parent::output($val, $rw);
		echo $rw?"<input name=\"$this->htmlname\"".($val===false?'':'value="'.htmlize($val).'"').($this->size?' size="'.$this->size.'"':'').' />':($val===false?'':htmlize($val));
		echo "<br/>\n";
	}
}
?>
