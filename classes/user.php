<?php
namespace WebFrame;
class user extends \DB\Row {
	public function name($new=false) {
		if ($new === false) {
			return $this->first.($this->middle?' '.$this->middle:'').' '.$this->last;
		} else {
			$parts=explode(' ', trim($new));
			$this->first=array_shift($parts);
			if ($this->first == '') {
				$this->first=null;
			}
			if ($parts) {
				$this->last=array_pop($parts);
			} else {
				$this->last=null;
			}
			if ($parts) {
				$this->middle=implode(' ', $parts);
			} else {
				$this->middle=null;
			}
		}
	}
	public function setPassword($newpass) {
		$this->passhash=hashPassword($newpass);
	}
	public function verifyPassword($pass) {
		return verifyPassword($pass, $this->passhash);
	}
	public function getAllOptions($obj=false) {
		$opts=array();
		$r=user_opt::select_by_user($this);
		while ($opt=$r->fetch()) {
			if ($obj) {
				$opts[$opt->key][]=$opt;
			} else {
				$opts[$opt->key][]=$opt->value;
			}
		}
		return $opts;
	}
	public function getOptionSingle($key, $obj=false) {
		$opt=user_opt::select_by_user_AND_key($this, $key)->fetch();
		if (!$opt) {
			return null;
		} elseif ($obj) {
			return $opt;
		} else {
			return $opt->value;
		}
	}
	public function getOptionMultiple($key, $obj=false) {
		$val=array();
		$r=user_opt::select_by_user_AND_key($this, $key);
		while ($opt=$r->fetch()) {
			if ($obj) {
				$val[]=$opt;
			} else {
				$val[]=$opt->value;
			}
		}
		return $val;
	}
	public function setOption($key, $value) {
		$opt=$this->getOptionSingle($key, true);
		if (isset($opt)) {
			$opt->value=$value;
			$opt->write();
		} else {
			return $this->addOption($key, $value);
		}
	}
	public function addOption($key, $value) {
		$opt=new user_opt();
		$opt->user=$this->id;
		$opt->key=$key;
		$opt->value=$value;
		$opt->write();
	}
	public function deleteOption($key, $value=null) {
		return query_db('DELETE FROM `user_opts` WHERE `user` = ? AND `key` = ?'.(isset($value)?' AND `value` = ? LIMIT 1':''), isset($value)?array(user(), $key, $value):array(user(), $key))->rowCount();
	}
	public function hasOption($key, $value=null) {
		if (isset($value)) {
			return user_opt::select_by_user_AND_key_AND_value(user(), $key, $value)->fetch() !== false;
		} else {
			return user_opt::select_by_user_AND_key(user(), $key)->fetch() !== false;
		}
	}
	public static function fromEmail($email) {
		$u=self::select_by_email($email)->fetch();
		if (!$u) {
			$u=additional_email::select_by_email($email)->fetch();
			if ($u) {
				$u=$u->get_user();
			}
		}
		if ($u) {
			return $u;
		} else {
			return false;
		}
	}
}
?>
