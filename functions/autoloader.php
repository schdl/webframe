<?php
namespace WebFrame;
function autoloader($class) {
	if (isset(Core::$cache)) {
		if (isset(Core::$cache['autoloader'][$class])) {
			require_once(Core::$cache['autoloader'][$class]);
		}
	} else {
		throw new \RuntimeException('Cache.json was not loaded properly - did you forget to run gencache?');
	}
}
?>
