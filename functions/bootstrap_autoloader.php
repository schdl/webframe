<?php
namespace WebFrame;
function bootstrap_autoloader($class) {
	$separator=strpos($class, '\\', 1);
	if ($separator === false) {
		return;
	}
	$module=substr($class, 0, $separator);
	$file=substr($class, $separator+1);
	if (substr($file, 0, 1) == 'p' && ctype_digit(substr($file, 1))) {
		$file=substr($file, 1);
	}
	if (strlen($file) == 0) {
		return;
	}
	$file=str_replace('__', '/', $file).'.php';
	$dirs=classPath($module);
	foreach ($dirs as $dir) {
		if (is_file($dir.'/'.$file)) {
			require_once($dir.'/'.$file);
			if (class_exists($class)) {
				break;
			} else {
				debug(__FUNCTION__, 'Loaded '.$dir.'/'.$file.' but '.$class.' is still not defined');
			}
		}
	}
}
?>
