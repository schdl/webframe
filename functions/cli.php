<?php
namespace WebFrame;
function isCLI() {
	return \PHP_SAPI === 'cli';
}
function isTTY() {
	static $cache=null;
	if ($cache === null) {
		if (isCLI()) {
			$stdout=fopen('php://stdout', 'r');
			$cache=posix_isatty($stdout);
		} else {
			$cache=false;
		}
	}
	return $cache;
}
?>
