<?php
namespace WebFrame;
function getConf($key) {
	if (array_key_exists($key, Core::$conf)) {
		return Core::$conf[$key];
	} else {
		return null;
	}
}
function setConf($key, $val) {
	Core::$conf[$key]=$val;
}
?>
