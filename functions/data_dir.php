<?php
namespace WebFrame;
function data_dir($module, $subdir=null) {
	$dir=getConf('data_dir').'/'.$module;
	if (isset($subdir)) {
		$dir.='/'.$subdir;
	}
	if (!is_dir($dir)) {
		debug(__FUNCTION__, 'Creating data directory '.$dir);
		if (!mkdir($dir, 0700, true) || !is_dir($dir)) {
			throw new RuntimeException('Failed to create directory '.$dir);
		}
	}
	return $dir;
}
?>
