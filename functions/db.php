<?php
namespace WebFrame;
function connectToDB($usedb=true) {
	static $connected=false;
	if (!$connected) {
		try {
			Core::$db=new \PDO('mysql:host='.getConf('dbhost').($usedb?';dbname='.getConf('dbname'):''), getConf('dbuser'), getConf('dbpass'), array(\PDO::ATTR_PERSISTENT => true));
		} catch (\Exception $e) {
			fatal(print_error('Database connection failure', $e->getMessage()));
		}
		// TODO Figure out a way to allow multiple database connections and coordinate them with DB* classes
		Core::$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		$connected=true;
	}
}
function query_db($sql, $bind=array(), $usecache=true) {
	connectToDB();
	if (!is_array($bind)) {
		$bind=array($bind);
	}
	foreach ($bind as &$obj) {
		if (is_a($obj, 'DB\\Row')) {
			$obj=$obj->primary_key();
		} elseif (is_a($obj, 'DateTime')) {
			$obj=$obj->format('Y-m-d H:i:s');
		}
	}
	static $cache=array();
	$new=true;
	$q=null;
	try {
		if ($usecache) {
			if (isset($cache[$sql])) {
				$new=false;
				$cache[$sql]->closeCursor();
			} else {
				$cache[$sql]=Core::$db->prepare($sql);
			}
			$q=&$cache[$sql];
		} else {
			$q=Core::$db->prepare($sql);
		}
	} catch (\Exception $e) {
		debug('prepare query failed', $sql);
		throw $e;
	}
	try {
		$q->execute($bind);
	} catch (\Exception $e) {
		debug('query failed', $sql."\nBind parameters: ".var_export($bind, true));
		throw $e;
	}
	debug('query', ['cached' => !$new, 'query' => $sql, 'rows' => $q->rowCount(), 'params' => $bind]);
	return $q;
}
function lastInsertId() {
	connectToDB();
	return Core::$db->lastInsertId();
}
function beginTransaction() {
	connectToDB();
	return Core::$db->beginTransaction();
}
function commit() {
	connectToDB();
	return Core::$db->commit();
}
function rollBack() {
	connectToDB();
	return Core::$db->rollBack();
}
?>
