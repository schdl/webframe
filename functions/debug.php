<?php
namespace WebFrame;
function debug($type, $text=null) {
	static $file=null;
	if (getConf('debug_log')) {
		if ($file === null) {
			$file=@fopen(getConf('debug_log_file'), 'a');
			if (!is_resource($file)) {
				debug('debug', 'Unable to open '.getConf('debug_log_file').' for writing');
			}
		}
		if (is_resource($file)) {
			if ($text === null) {
				fputs($file, json_encode($type)."\n");
			} else {
				fputs($file, $type.': '.json_encode($text)."\n");
			}
		}
	}
	if ($text===null) {
		$item=$type;
	} else {
		$item=[$type => $text];
	}
	if (Core::$debug === false) {
		if (getConf('debug')) {
			runTemplate('debug', ['debug' => [$item]]);
		}
	} else {
		// Avoid unbounded memory use
		if (count(Core::$debug) === 10000) {
			Core::$debug=array();
			debug('debug', 'Truncated debug at 10,000 lines');
		}
		Core::$debug[]=$item;
	}
}
?>
