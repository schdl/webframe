<?php
namespace WebFrame;
function triggerEvent($type, $data) {
	$type.='_listeners';
	if (isset(Core::$cache[$type])) {
		foreach (Core::$cache[$type] as $listener) {
			$listener['callback']($data);
		}
	}
}
?>
