<?php
namespace WebFrame;
function exception_handler($e) {
	debug('exception', $e->getMessage()."\n".$e->getTraceAsString());
	if (getConf('debug') && !headers_sent()) {
		header('X-Schdl-Exception: '.json_encode($e->getMessage()."\n".$e->getTraceAsString()), false);
	}
	$page=findPage('500');
	if ($page) {
		$page->run();
	}
}
?>
