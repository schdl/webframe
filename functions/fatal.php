<?php
namespace WebFrame;
function fatal($msg) {
	header('HTTP/1.1 500 Internal Server Error', true, 500);
	if (isCLI()) {
		echo $msg;
	} else {
		echo htmlize($msg);
	}
	die;
}
?>
