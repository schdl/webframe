<?php
namespace WebFrame;
function findObject($type, $name, $justone=false) {
	$name=(strpos($name, '/', 1) === false && ctype_digit($name)?'p':'').str_replace('/', '__', $name);
	$opts=isset(Core::$cache[$type][$name])?Core::$cache[$type][$name]:[];
	prioritize($opts);
	if ($justone) {
		if ($opts) {
			$obj=new $opts[0]['class']();
			return $obj;
		} else {
			return null;
		}
	} else {
		$objs=[];
		foreach ($opts as $opt) {
			$obj=new $opt['class']();
			$objs[]=$obj;
		}
		return $objs;
	}
}
?>
