<?php
namespace WebFrame;
// Generates colors suitable for the background of black text - for foreground, try using flip_color to invert them
function gencolor($name, $choices=null) {
	static $next=[];
	if (!$choices) {
		$choices=['#008000', '#CCCCFF', '#00CCFF', '#CCFFCC', '#FFFF99', '#99CCFF', '#FF99CC' ,'#CC99FF', '#FFCC99', '#3366FF', '#33CCCC', '#99CC00', '#FFCC00', '#FF9900', '#FF6600', '#666699', '#969696', '#339966', '#993300', '#008080', '#C0C0C0', '#808080', '#9999FF', '#CCFFFF', '#FF8080', '#0066CC', '#FF0000', '#00FF00', '#FFFF00', '#FF00FF', '#00FFFF'];
	}
	$hash=md5(implode("\n", $choices));
	$my_next=&$next[$name][$hash];
	if (!isset($my_next)) {
		$my_next=0;
	}
	$color=$choices[$my_next];
	$my_next=($my_next+1)%count($choices);
	return $color;
}
// Inverts colors of the form '#xxxxxx'
function flip_color($color) {
	$c=dechex(hexdec('ffffff')-hexdec($color));
	while (strlen($c) < 6) {
		$c="0$c";
	}
	return '#'.$c;
}
?>
