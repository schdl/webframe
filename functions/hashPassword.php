<?php
namespace WebFrame;
function hashPassword($pass) {
	$salt='$2a$'.getConf('blowfish_cost').'$'.randstring(22, './0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
	$hash=crypt($pass, $salt);
	if (strlen($hash) < 13) {
		trigger_error('Password hashing failed: used salt '.$salt.' and got result '.$hash, \E_USER_ERROR);
	}
	return $hash;
}
?>
