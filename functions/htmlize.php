<?php
namespace WebFrame;
function htmlize($str) {
	return htmlentities($str, \ENT_QUOTES|\ENT_SUBSTITUTE, 'UTF-8', true);
}
?>
