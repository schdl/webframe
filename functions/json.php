<?php
namespace WebFrame;
function json_error_string() {
	switch(json_last_error()) {
		case \JSON_ERROR_NONE:
			return null;
		case \JSON_ERROR_DEPTH:
			return 'The maximum stack depth has been exceeded';
		case \JSON_ERROR_STATE_MISMATCH:
			return 'Invalid or malformed JSON';
		case \JSON_ERROR_CTRL_CHAR:
			return 'Control character error, possibly incorrectly encoded';
		case \JSON_ERROR_SYNTAX:
			return 'Syntax error';
		case \JSON_ERROR_UTF8:
			return 'Malformed UTF-8 characters, possibly incorrectly encoded';
		default:
			return 'Unknown error';
	}
}
function json_encode_attr($string) {
	$string=(string)$string;
	return '\''.str_replace('\'', '\\\'', substr(json_encode($string), 1, -1)).'\'';
}
?>
