<?php
namespace WebFrame;
// TODO Add coloring if outputting to a terminal
function print_message_long($type, $title, $message) {
	if (isCLI()) {
		return "$type: $title: $message\n";
	} else {
		return bufferTemplate('messagebox', array('title' => $title, 'msg' => $message, 'type' => $type));
	}
}
function print_message_short($type, $message) {
	if (isCLI()) {
		return "$type: $message\n";
	} else {
		return bufferTemplate('messagebox', array('msg' => $message, 'type' => $type));
	}
}
function print_error($title, $message=null) {
	return $message==null?print_message_short('error', $title):print_message_long('error', $title, $message);
}
function print_warning($title, $message=null) {
	return $message==null?print_message_short('warning', $title):print_message_long('warning', $title, $message);
}
function print_success($title, $message=null) {
	return $message==null?print_message_short('success', $title):print_message_long('success', $title, $message);
}
?>
