<?php
namespace WebFrame;
function moduleConf($module, $key) {
	if (array_key_exists($module, Core::$modules) && array_key_exists($key, Core::$modules[$module])) {
		return Core::$modules[$module][$key];
	} else {
		return null;
	}
}
function getModuleDirIfExists($module, $conf_key) {
	$root=moduleConf($module, 'root');
	if (!$root) {
		return null;
	}
	$dir=moduleConf($module, $conf_key);
	if (!$dir) {
		return null;
	}
	$dir=$root.'/'.$dir;
	if (is_dir($dir)) {
		return $dir;
	} else {
		return null;
	}
}
function functionPath($module) {
	return getModuleDirIfExists($module, 'function_path');
}
function cssPath($module) {
	return getModuleDirIfExists($module, 'css_path');
}
function staticPath($module=null) {
	return getModuleDirIfExists($module, 'static_path');
}
function templateDir($module=null) {
	return getModuleDirIfExists($module, 'template_dir');
}
function classPath($module) {
	$root=moduleConf($module, 'root');
	if (!$root) {
		return [];
	}
	$cp=moduleConf($module, 'class_path');
	if (!$cp) {
		return [];
	}
	$cp=explode(':', $cp);
	$path=[];
	foreach ($cp as $dir) {
		$dir=$root.'/'.$dir;
		if (is_dir($dir)) {
			$path[]=$dir;
		}
	}
	return $path;
}
function moduleRoot($module) {
	return moduleConf($module, 'root');
}
?>
