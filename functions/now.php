<?php
namespace WebFrame;
function now() {
	static $tz=null;
	if (!$tz) {
		$tz=new \DateTimeZone('UTC');
	}
	return new \DateTime('now', $tz);
}
