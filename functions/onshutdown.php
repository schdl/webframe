<?php
namespace WebFrame;
function onshutdown() {
	if (!isCLI()) {
		$error=error_get_last();
		if ($error && getConf('debug')) {
			debug('error', $error['type'].': '.$error['message'].' @ '.$error['file'].' '.$error['line']);
		}
		if (isset(Core::$awaiting_footer)) {
			runTemplate(Core::$awaiting_footer.'\\footer', StandardPage::standardData());
		}
	}
}
?>
