<?php
namespace WebFrame;
function page($newval=null) {
	if ($newval === null) {
		if (!isset(Core::$page)) {
			Core::$page=new Page();
		}
		return Core::$page;
	} else {
		Core::$page=$newval;
	}
}
/**
 * @param string $page the page specifier string
 * @returns WebFrame\Page an instance of the first corresponding page class, or null if no class was found
 */
function findPage($page, $args=null, $justone=true) {
	if ($args === null) {
		$args=array();
	}
	$pages=findObject('pages', $page, $justone);
	if ($justone) {
		$pages->args=array_merge($pages->args, $args);
	} else {
		foreach ($pages as $page) {
			$page->args=array_merge($page->args, $args);
		}
	}
	return $pages;
}
?>
