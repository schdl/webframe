<?php
namespace WebFrame;
function print_columns(array $data, $columns, $class=null) {
	echo '<table'.($class === null?'':' class="'.$class.'"').'>';
	$n=count($data);
	$rows=ceil($n/$columns);
	$table=array();
	for ($i=0; $i<$rows; $i++) {
		$table[$i]='';
	}
	$i=0;
	foreach ($data as $d) {
		$table[$i++%$rows].='<td>'.$d.'</td>';
	}
	foreach ($table as $row) {
		echo '<tr>'.$row.'</tr>'."\n";
	}
	echo '</table>';
}
?>
