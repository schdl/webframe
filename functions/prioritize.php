<?php
namespace WebFrame;
/*
 * Implements a stable sort based on precedence (semi-bucket sort using k(r)sort on the buckets)
 * 
 * If reverse is true, result goes from high to low precedence, otherwise from low to high,
 * but either way items with the same precedence maintain their original order
 */
function prioritize(&$items, $reverse=true) {
	if (count($items) < 2) {
		return;
	}
	$buckets=[];
	// Only sort if at least one item has a defined precedence !== 50
	$worth_sorting=false;
	foreach ($items as $key => $item) {
		if (isset($item['precedence']) && $item['precedence'] !== 50) {
			$buckets[$item['precedence']][$key]=$item;
			$worth_sorting=true;
		} else {
			$buckets[50][$key]=$item;
		}
	}
	if (!$worth_sorting) {
		return;
	}
	if ($reverse) {
		krsort($buckets);
	} else {
		ksort($buckets);
	}
	$items=[];
	foreach ($buckets as $precedence => $bucket) {
		foreach ($bucket as $key => $item) {
			if (is_int($key)) {
				$items[]=$item;
			} else {
				$items[$key]=$item;
			}
		}
	}
}
?>
