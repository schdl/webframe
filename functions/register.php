<?php
namespace WebFrame;
function register($type, $obj) {
	if (isset($obj['name'])) {
		Core::$cache[$type][$obj['name']]=$obj;
	} else {
		Core::$cache[$type][]=$obj;
	}
}
function registerRoute($route) {
	$route['class']=Core::$context;
	if (!isset($route['name'])) {
		list(,$name)=explode('\\', str_replace('__', '/', Core::$context), 2);
		if (!isset(Core::$cache['routes'][$name])) {
			$route['name']=$name;
		}
	}
	register('routes', $route);
}
function registerCallback($type, $callback, $opts=[]) {
	if (is_string($callback)) {
		switch(contextType()) {
		case 'class':
			$callback=[Core::$context, $callback];
			break;
		case 'module':
			$callback=Core::$context.'\\'.$callback;
			break;
		}
	}
	register($type, array_merge($opts, ['callback' => $callback]));
}
function registerRegistrar($callback='register', $opts=[]) {
	registerCallback('registrars', $callback, $opts);
}
function registerInit($callback='init', $opts=[]) {
	registerCallback('initializers', $callback, $opts);
}
function registerClass($type, $opts=[], $single=false) {
	$opts['class']=Core::$context;
	if (isset($opts['name'])) {
		$name=$opts['name'];
		unset($opts['name']);
	} else {
		list(, $name)=explode('\\', Core::$context, 2);
	}
	if ($single) {
		Core::$cache[$type][$name]=$opts;
	} else {
		Core::$cache[$type][$name][]=$opts;
	}
}
function registerEventListener($type, $callback, $opts=[]) {
	registerCallback($type.'_listeners', $callback, $opts);
}
function contextType() {
	if (class_exists(Core::$context)) {
		return 'class';
	} else {
		return 'module';
	}
}
?>
