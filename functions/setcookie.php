<?php
namespace WebFrame;
function setcookie($name=null, $value='', $expiration=1, $path=null, $secure=false, $httponly=true) {
	if ($name === null) {
		$name=getConf('cookiename');
	}
	if ($path === null) {
		$path=getConf('http_path');
	}
	if ($path == '') {
		$path='/';
	}
	debug('setcookie', $name.'='.$value.' ('.$path.')');
	return \setcookie($name, $value, $expiration, $path, getConf('hostname'), $secure, $httponly);
}
?>
