<?php
namespace WebFrame;
function doRunTemplate($module, $template, $data=array()) {
	if ($template === 'footer' && $module !== 'WebFrame') {
		// Fall back to WebFrame\footer if this footer fails
		Core::$awaiting_footer='WebFrame';
	}
	if (($dir=templateDir($module)) === null) {
		throw new \RuntimeException('Module '.$module.' does not have a template directory');
	} elseif (!is_readable($file=$dir.'/'.$template.'.php')) {
		throw new \RuntimeException('Template not found or not readable at '.$file);
	} elseif ($template === 'header' && !isset(Core::$awaiting_footer)) {
		Core::$awaiting_footer=is_readable($dir.'/footer.php')?$module:'WebFrame';
	}
	array_unshift(Core::$template_state, array('module' => $module, 'template' => $template, 'file' => $file, 'data' => $data));
	unset($module, $template, $dir, $file);
	if (array_key_exists('data', $data)) {
		extract($data);
	} else {
		extract($data);
		unset($data);
	}
	try {
		$r=include(Core::$template_state[0]['file']);
		$state=array_shift(Core::$template_state);
		if ($state['template'] === 'footer') {
			// Footer was run - don't do it again
			Core::$awaiting_footer=null;
		}
		return $r;
	} catch (\Exception $e) {
		array_shift(Core::$template_state);
		throw $e;
	}

}
function parentTemplate($data=null) {
	if (!Core::$template_state) {
		throw new \RuntimeException('No template is currently running');
	}
	$state=Core::$template_state[0];
	if (!isset($data)) {
		$data=$state['data'];
	}
	$parent_module=findParentTemplate($state['module'], $state['template']);
	return doRunTemplate($parent_module, $state['template'], $data);
}
function findParentTemplate($module, $template) {
	if (!isset(Core::$cache['templates'][$template])) {
		throw new \RuntimeException('Template '.$template.' not found');
	}
	$found=false;
	$parent_module=null;
	foreach (Core::$cache['templates'][$template] as $mod) {
		if ($found) {
			$parent_module=$mod;
			break;
		} elseif ($mod === $module) {
			$found=true;
		}
	}
	if ($parent_module === null) {
		if ($found) {
			throw new \RuntimeException('Template '.$module.'\\'.$template.' has no parent');
		} else {
			throw new \RuntimeException('Template '.$module.'\\'.$template.' not found');
		}
	}
	return $parent_module;
}
function runTemplate($name, $data=null) {
	if ($data === null) {
		if (count(Core::$template_state)) {
			$data=Core::$template_state[0]['data'];
		} else {
			$data=array();
		}
	}
	if (strpos($name, '\\') !== false) {
		list($module, $template)=explode('\\', $name);
		return doRunTemplate($module, $template, $data);
	} elseif (isset(Core::$cache['templates'][$name])) {
		return doRunTemplate(Core::$cache['templates'][$name][0], $name, $data);
	} else {
		throw new \RuntimeException('Template '.$name.' not found');
	}
}
function bufferTemplate($name, $data=null, &$return_val=null) {
	ob_start();
	try {
		$return_val=runTemplate($name, $data);
		return ob_get_clean();
	} catch (\Exception $e) {
		ob_get_clean();
		throw $e;
	}
}
?>
