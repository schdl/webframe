<?php
namespace WebFrame;
function url($url='', $secure=null) {
	if ($secure === null) {
		$secure=isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on';
	}
	// Idea: just use // - apparently it is valid and means to use the same protocol that the page was loaded with
	return ($secure?'https://':'http://').getConf('hostname').getConf('http_path').htmlize($url);
}
