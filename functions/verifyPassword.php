<?php
namespace WebFrame;
function verifyPassword($pass, $hash) {
	return crypt($pass, $hash) === $hash;
}
?>
