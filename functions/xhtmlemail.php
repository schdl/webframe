<?php
namespace WebFrame;
// Sends an XHTML email with the appropriate headers and the necessary opening and closing for an XHTML document
function xhtmlemail($to,$from,$subj,$cont,$inheads=null) {
	if ($from===null) {
		$from=getConf('emailfrom');
	}
	$heads='MIME-Version: 1.0' . "\r\n";
	$heads.='Content-type: text/html; charset=utf-8' . "\r\n";
	$heads.='From: '.$from."\r\n";
	$heads.='X-Mailer: PHP/'.getConf('title')."\r\n";
	if ($inheads!==null) {
		$heads.=$inheads."\r\n";
	}
	$cont='<?xml version="1.0" encoding="utf-8"?>'."\n".'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">'."\n".'<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"><body>'.$cont.'</body></html>'."\n";
	$heads.='Content-length: '.strlen($cont)."\r\n";
	debug('mail', $heads.$cont);
	return mail($to,$subj,$cont,$heads);
}
?>
