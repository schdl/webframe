<?php
namespace WebFrame;
//define('E_DEFAULT', error_reporting(E_ALL)); // Removed |E_STRICT
require_once(__DIR__.'/Core.php');
foreach (Core::$cache['routes'] as $route) {
	$route=new Route($route);
	if ($page=$route->match(request(), $_SERVER['REQUEST_METHOD'], $_GET, $_POST)) {
		debug(__FILE__, 'Page args='.var_export($page->args, true));
		$next=true;
		while ($next !== null) {
			debug('routing','Routing to '.get_class($page));
			page($page);
			$next=$page->init();
			if (is_array($next)) {
				$args=$next;
				$next=$args['page'];
				unset($args['page']);
			} else {
				$args=[];
			}
			if (isset($next)) {
				$page=findPage($next, $args);
				if ($page === null) {
					$page=findPage('500');
					if ($page === null) {
						fatal(print_error('Routing Failure','The page you are trying to reach, <i>'.url(request()).'</i> does not exist. Additionally, the 500 error page could not be found.'));
					}
				}
			}
		}
		$page->header();
		$page->body();
		$page->footer();
		die;
	}
}
fatal(print_error('Routing Failure', 'You should never see this message.  Please contact an administrator.'));
?>
