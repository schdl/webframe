<?php
namespace WebFrame;
class p301 extends ErrorPage {
	public $title='Page Permanently Moved';
	protected static $code=301, $msg='Moved Permanently', $type='warning';
	function init() {
		parent::init();
		header('Location: '.$this->args['url']);
	}
	function getMessage() {
		return 'The page you are looking for has been permanently moved to <a href="'.htmlize($this->url).'">'.htmlize($this->url).'</a>. Please update any bookmarks or links that you might have.';
	}
}
?>
