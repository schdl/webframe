<?php
namespace WebFrame;
class p302 extends p301 {
	public $title='Page Moved';
	protected static $code=302, $msg='Found';
	function getMessage() {
		return 'If you are not automatically redirected, click <a href="'.htmlize($this->args['url']).'">here</a> to continue.';
	}
}
?>
