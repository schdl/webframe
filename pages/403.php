<?php
namespace WebFrame;
class p403 extends ErrorPage {
	public $title='Forbidden';
	protected static $code=403, $msg='Forbidden', $type='error';
	function getMessage() {
		return 'You do not have permission to access <i>'.htmlize(url().'/'.request()).'</i>.';
	}
}
?>
