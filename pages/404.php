<?php
namespace WebFrame;
class p404 extends ErrorPage {
	public $title='Page Not Found';
	public static $precedence=0;
	public static $routing=array('.*' => null);
	protected static $code=404, $msg='Not Found', $type='error', $msg_title='Page Not Found!';
	function getMessage() {
		return 'The page you are trying to reach, <i>'.htmlize(url(request())).'</i> does not exist.';
	}
}
?>
