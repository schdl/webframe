<?php
namespace WebFrame;
class p500 extends ErrorPage {
	public $title='Internal Server Error';
	protected static $code=500, $msg='Internal Server Error', $type='error', $msg_title='Oops!';
	function getMessage() {
		return 'Something went wrong. We\'ll try to fix it as soon as possible, so please try again later.';
	}
}
?>
