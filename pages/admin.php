<?php
namespace WebFrame;
class admin extends AdminPage {
	public $title='Admin';
	public static $routing=array('admin' => null);
	function body() {
		echo '<h1>Administration</h1>';
		foreach (findPage('include/admin', null, false) as $extra) {
			$extra->run();
		}
	}
}
?>
