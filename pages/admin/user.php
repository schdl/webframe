<?php
namespace WebFrame;
class admin__user extends AdminPage {
	public static $routing=array('admin/users/([0-9]+)' => array('user'));
	protected $msg=array();
	function _init() {
		$this->user=user::select_by_id($this->args['user'])->fetch();
		if ($this->user === false) {
			return '404';
		}
		if (isset($_POST['go'])) {
			$this->user->first=$_POST['first'];
			$this->user->middle=$_POST['middle'];
			$this->user->last=$_POST['last'];
			if ($this->user->id == user()->id && !isset($_POST['is_admin'])) {
				$this->msg[]='noselfdemotion';
			} else {
				$this->user->set_flag('a', isset($_POST['is_admin']));
			}
			if ($_POST['newpass']) {
				if ($_POST['newpass'] == $_POST['newpass2']) {
					$this->user->setPassword($_POST['newpass']);
				} else {
					$this->msg[]='passnotmatch';
				}
			}
			$this->user->write();
			$this->msg[]='updated';
		}
		$this->title=$this->user->name();
	}
	function body() {
		echo '<h1>User: '.htmlize($this->user->name());
		if (getConf('allow_su')) {
			$path=getConf('http_path')?getConf('http_path'):'/';
			echo ' <a href="javascript:void(0)" onclick="document.cookie=\''.getConf('cookiename').'_su='.$this->user->id.'; path='.$path.'\'; document.location.href=\''.url().'\'"><img class="inline" src="'.static_content::url(array('file' => 'images/su.png')).'" alt="[su]" /></a>';
		}
		echo '</h1>', "\n";
		foreach ($this->msg as $msg) {
			switch($msg) {
				case 'updated':
					echo print_success('User updated.');
					break;
				case 'noselfdemotion':
					echo print_warning('You may not remove your own admin status.');
					break;
				case 'nopassmatch':
					echo print_error('The passwords that you entered did not match.');
					break;
			}
		}
		echo '<form method="post">First: <input name="first" value="'.htmlize($this->user->first).'" /><br/>';
		echo 'Middle: <input name="middle" value="'.htmlize($this->user->middle).'" /><br/>';
		echo 'Last: <input name="last" value="'.htmlize($this->user->last).'" /><br/>';
		echo 'Change password: <input type="password" name="newpass" /><br/>';
		echo 'Repeat new password: <input type="password" name="newpass2" /><br/>';
		echo 'Is admin? <input type="checkbox" name="is_admin"'.($this->user->has_flag('a')?' checked="checked"':'').' /><br>';
		echo '<input type="submit" name="go" value="Update User" />';
		echo '<p><a href="'.admin__users::url().'">Back to Users</a></p>';
	}
}
?>
