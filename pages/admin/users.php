<?php
namespace WebFrame;
class admin__users extends AdminPage {
	public static $routing=array('admin/users' => null);
	public $title='Users';
	function body() {
		echo '<h1>Users:</h1>';
		if (isset($_GET['q'])) {
			$q='%'.$_GET['q'].'%';
			$r=user::wrapped_query('SELECT * FROM `users` WHERE CONCAT_WS(" ", `first`, `middle`, `last`) LIKE ? OR `email` LIKE ? ORDER BY `last`, `first`, `middle`', array($q, $q));
			$users=array();
			while ($user=$r->fetch()) {
				$users[]='<a href="'.admin__user::url(array('user' => $user->id)).'">'.htmlize($user->name()).'</a>';
			}
			print_columns($users, 4);
		}
		echo '<p><form method="get">Search: <input name="q" /> <input type="submit" value="Search" /></form></p>';
		echo '<p><a href="'.admin::url().'">Back to Administration</a></p>';
	}
}
?>
