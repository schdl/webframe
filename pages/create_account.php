<?php
namespace WebFrame;
class create_account extends StandardPage {
	public $title='Create Account';
	public static $routing=array('create-account' => null);
	protected $msg=null, $success=false;
	function init() {
		if (user()) {
			return array('page' => '302', 'url' => welcome::url());
		} elseif (!getConf('registration')) {
			return '403';
		}
		if (isset($_POST['email'])) {
			if (EmailValidator::create()->validate($_POST['email'])) {
				if (user::fromEmail($_POST['email'])) {
					$this->msg=print_error('An account with that email address already exists. Please try '.login::link('logging in').'.');
				} else {
					$ev=email_verification::create('create_account', new \DateInterval('P1D'), $_POST['email']);
					xhtmlemail($_REQUEST['email'], null, getConf('title').' account creation', 'To complete your account registration, click this link: '.register::link(null, 'key='.$ev->key).'. This link will expire in 24 hours.');
					$this->success=true;
				}
			} else {
				$this->msg=print_warning('Please enter a valid email address.');
			}
		}
		$this->description='Create an account on '.getConf('title');
	}
	function body() {
		echo '<h2>'.$this->title.'</h2>';
		if ($this->success) {
			echo print_success('You will receive an email soon at '.htmlize($_REQUEST['email']).' with instructions to finish creating your account.');
		} else {
			if (isset($this->msg)) {
				echo $this->msg;
			}
			echo '<p>Once you confirm your email address, you will be able to create your '.getConf('title').' account.</p><form action="'.self::url().'" method="post">E-mail address: <input name="email" /> <input type="submit" value="Submit" /></form>';
		}
	}
	public static function sitemapURLs() {
		if (getConf('registration')) {
			return static::defaultSitemapURL();
		}
	}
}
