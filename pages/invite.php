<?php
namespace WebFrame;
class invite extends StandardPage {
	public $title='Invite';
	public static $routing=array('invite' => null);
	static function isAllowed() {
		return getConf('invite');
	}
	function init() {
		if (user() === null) return 'login';
		if (getConf('invite') === 'admin' && !user()->has_flag('a')) {
			return '403';
		}
		$this->description='Invite your friends to join '.getConf('title');
	}
	function body() {
		if (isset($_REQUEST['emails'])) {
			echo '<h3>Inviting Users</h3>';
			$emails=explode("\n", $_REQUEST['emails']);
			$sent=0;
			foreach ($emails as $email) {
				$email=trim($email);
				if (strlen($email) == 0) {
					continue;
				}
				if (!EmailValidator::create()->validate($email)) {
					echo 'Email address "'.htmlize($email).'" is invalid<br/>';
					continue;
				}
				$tz=new \DateTimeZone('utc');
				$now=new \DateTime('now', $tz);
				if (email_verification::wrapped_query('SELECT * FROM `email_verifications` WHERE type = ? AND email = ? AND user = ? AND `expiration` >= ?', array('create_account', $email, user(), $now->format('Y-m-d H:i:s')))->fetch()) {
					echo 'You have already sent an invitation to '.htmlize($email).'<br/>';
					continue;
				}
				$sent++;
				if (user::fromEmail($email)) {
					continue;
				}
				$ev=email_verification::create('create_account', new \DateInterval('P1D'), $email, user());
				if (!xhtmlemail($email, null, getConf('title').' invitation', '<a href="mailto:'.htmlize(user()->email).'">'.htmlize(user()->name()).'</a> has invited you to  use '.getConf('title').'. Try it out at '.findPage('welcome')->link(null).' and if you like what you see, create an account at '.register::link(null, array('key' => $ev->key)).' (this link will expire in 24 hours'.(getConf('registration')?' but you can always create an account at '.create_account::link():'; after that, you will have to ask '.htmlize(usr()->name()).' to send you another invitation').').', 'Reply-To: '.user()->email)) {
					echo 'Unable to invite '.htmlize($email).' - sending of email failed.<br/>';
					$ev->delete();
					$sent--;
				}
			}
			if ($sent) {
				echo print_success('Your invitation'.($sent>1?'s have':' has').' been sent.');
			}
			echo '<a href="'.static::url().'">Send more invitations</a>';
		} else {
			echo '<h3>Invite Users</h3><form action="'.static::url().'" method="post">Email addresses to send invitations to: (one per line)<br/><textarea name="emails"></textarea><br/><input type="submit" value="Send Invitations" /></form>';
		}
	}
}
?>
