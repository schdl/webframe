<?php
namespace WebFrame;
class login extends StandardPage {
	public $title='Login';
	public static $routing=array('login' => array(), 'login/(.+)' => array('go'));
	function init() {
		if (!isset($this->args['go']) && substr(request(), 0, 5) != 'login') {
			$this->args['go']=static::defaultGo();
			$this->bodyData['please_sign_in']=true;
		} else {
			if (isset($_POST['go'])) {
				$this->args['go']=$_POST['go'];
			}
			$this->bodyData['please_sign_in']=false;
		}
		$this->bodyData['title']='Login';
		$form=(new \Forms\Form())
			->method('post')
			->action(self::url($this->args))
			->add((new \Forms\Input())
				->name('email')
				->label('Email: '))
			->add((new \Forms\Password())
				->name('password')
				->label('Password: '))
			->add((new \Forms\Submit())
				->text('Sign in'));
		$this->bodyData['form']=$form;
		$data=$form->val();
		$this->bodyData['data']=&$data;
		if (isset($data['email'])) {
			$user=user::fromEmail($data['email']);
			if ($user === false) {
				$this->bodyData['result']=false;
			} elseif ($user->verifyPassword($data['password'])) {
				Core::$user=$user;
				$this->bodyData['result']=session::create();
				$this->bodyData['form']=false;
				if (getConf('blowfish_update_hashes')) {
					if (substr($user->passhash, 4, 2) != getConf('blowfish_cost')) {
						debug(__METHOD__, 'Updating pass hash to new cost');
						$user->setPassword($this->data['password']);
						$user->write();
					}
				}
			} else {
				$this->bodyData['result']=false;
			}
		}
		if (user() !== null) {
			return array('page' => '302', 'url' => isset($this->args['go'])?url($this->args['go']):findPage('welcome')->url());
		}
		$this->description='Log in to '.getConf('title');
	}
	public static function defaultGo() {
		$url=request();
		if ($_GET) {
			$url.='?'.http_build_query($_GET);
		}
		return $url;
	}
}
?>
