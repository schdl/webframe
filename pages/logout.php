<?php
namespace WebFrame;
class logout extends StandardPage {
	public $title='Logout';
	public static $routing=array('logout' => null, 'logout/(.+)' => array('go'));
	function init() {
		if (user() !== null) {
			session()->delete();
		}
		setcookie();
		Core::$user=null;
		if (isset($this->args['go'])) {
			$this->bodyData['go']=$this->args['go'];
		}
	}
}
?>
