<?php
namespace WebFrame;
class menu extends StandardPage {
	function init() {
		$name=isset($this->args['name'])?$this->args['name']:'MainMenuGenerator';
		$generators=findObject('menus', $name);
		$items=[];
		foreach ($generators as $gen) {
			$items=array_merge($items, $gen->getMenuItems());
		}
		$this->bodyData['items']=$items;
	}
}
?>
