<?php
namespace WebFrame;
class register extends StandardPage {
	public $title='Register';
	public static $routing=array('register/([a-zA-Z0-9]{30})' => array('key'));
	protected $ev, $msg='', $showform=true;
	function init() {
		if (user() !== null) {
			return array('page' => '302', 'url' => findPage('welcome')->canonicalURL());
		}
		$this->ev=email_verification::validate('create_account', $this->args['key']);
		if (!$this->ev) {
			$this->msg.=print_warning('Your registration key was not found and may have expired. Please '.create_account::link('try again').'.');
			$this->showform=false;
			return;
		}
		if (user::fromEmail($this->ev->email)) {
			$this->msg.=print_warning('An account with your email address already exists. Please try '.login::link('logging in').'.');
			$this->showform=false;
			return;
		}
		if (isset($_POST['name'])) {
			if (strlen($_POST['name']) < 4) {
				$this->msg.=print_warning('Please enter your name.');
			}
			if (!isset($_POST['password']) || strlen($_POST['password']) <= 4) {
				$this->msg.=print_warning('Please enter a password at least five characters long.');
			} elseif (!isset($_POST['confirm']) || $_POST['confirm'] !== $_POST['password']) {
				$this->msg.=print_warning('The passwords that you entered do not match.');
			}
			if (!$this->msg) {
				$user=new user();
				$user->name($_POST['name']);
				$user->email=$this->ev->email;
				$user->setPassword($_POST['password']);
				$tz=new \DateTimeZone('utc');
				$now=new \DateTime('now', $tz);
				$user->created=$now->format('Y-m-d H:i:s');
				$fail=false;
				foreach (findObject('callbacks', 'PreRegistration') as $pr) {
					if ($pr($user, $this->msg) === false) {
						$fail=true;
						break;
					}
				}
				$user->write();
				Core::$user=$user;
				$this->ev->delete();
				session::create();
				$this->post_reg=findPage('include/post_registration', null, false);
				foreach ($this->post_reg as $page) {
					$page->init();
				}
			}
		}
	}
	function body() {
		if (user()) {
			echo print_success('Your account has been created. '.findPage('welcome')->link('Continue.'));
			foreach ($this->post_reg as $page) {
				$page->body();
			}
		} else {
			if ($this->msg) {
				echo $this->msg;
			}
			if ($this->showform) {
				echo '<h1>Register</h1><form method="post">Full Name: <input name="name"';
				if (isset($_REQUEST['name'])) {
					echo ' value="'.htmlize($_REQUEST['name']).'"';
				}
				echo '/><br/>Password: <input type="password" name="password" /><br/>Confirm Password: <input type="password" name="confirm" /><br/><input type="submit" value="Create Account" /></form>';
			}
		}
	}
}
?>
