<?php
namespace WebFrame;
class robots extends Page {
	public static $routing=array('robots\\.txt' => null);
	function init() {
		contenttype('text/plain');
	}
	function body() {
		foreach (findPage('include/robots', null, false) as $page) {
			$page->run();
			echo "\n";
		}
		echo <<<EOF
User-agent: *
Allow: /
EOF;
		echo "\n";
	}
}
?>
