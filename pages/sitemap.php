<?php
namespace WebFrame;
class sitemap extends Page {
	public static $routing=array('sitemap\\.xml' => null);
	function init() {
		contenttype('text/xml');
	}
	function body() {
		$routes=array();
		foreach (Core::$cache['routes'] as $route) {
			if (!isset($routes[$route['pattern']])) {
				$routes[$route['pattern']]=$route['class'];
			}
		}
		$classes=array();
		foreach ($routes as $class) {
			$classes[$class]=true;
		}
		echo xmlheader().'<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";
		$classes=array_keys($classes);
		foreach ($classes as $class) {
			$urls=$class::sitemapURLs();
			if (empty($urls)) {
				continue;
			} elseif (is_object($urls)) {
				$urls=array($urls);
			}
			foreach ($urls as $url) {
				echo $url->toXML();
			}
		}
		echo '</urlset>'."\n";
	}
}
?>
