<?php
namespace WebFrame;
class static_content extends Page {
	public static $routing=array(
		'(images/.*)' => array('file'),
		'(js/.*\\.js)' => array('file'),
		'(css/.*\\.css)' => array('file'),
		'(?:.*/|)favicon.ico' => array('file' => 'images/favicon.ico')
	);
	protected $path;
	protected $extension;
	function init() {
		if (strpos($this->args['file'], '..') !== false) {
			debug(__METHOD__, '.. found in filename '.$this->args['file']);
			return '404';
		} elseif (strtolower(substr($this->args['file'], -4)) == '.php') {
			debug(__METHOD__, '.php found in filename '.$this->args['file']);
			return '404';
		}
		foreach (modules() as $module) {
			$dir=staticPath($module);
			if (!isset($module)) {
				continue;
			}
			debug(__FUNCTION__, 'Checking for '.$dir.'/'.$this->args['file'].'(.php)?');
			if (is_file($dir.'/'.$this->args['file'].'.php')) {
				$this->args['file'].='.php';
			}
			if (is_file($dir.'/'.$this->args['file'])) {
				$this->path=$dir.'/'.$this->args['file'];
				$this->extension=strtolower(pathinfo($this->path, \PATHINFO_EXTENSION));
				if ($this->extension === 'php') {
					$mime=isset($this->args['mime'])?$this->args['mime']:mime_ext(substr($this->path, 0, -4));
					header('Content-Type: '.$mime);
				} else {
					$mime=isset($this->args['mime'])?$this->args['mime']:mime($this->path);
					header('Content-Type: '.$mime);
					$expires=new \DateTime(null, new \DateTimeZone('UTC'));
					$expires->add(new \DateInterval('PT'.getConf('static_cache_max_age').'S'));
					header('Expires: '.$expires->format(\DateTime::RFC1123));
					header('Cache-Control: max-age='.getConf('static_cache_max_age'));
					header('Content-Length: '.filesize($this->path));
				}
				if (isset($this->args['force_download']) && $this->args['force_download']) {
					header('Content-Description: File Transfer');
					header('Content-Transfer-Encoding: binary');
					if (isset($this->args['download_name']) && strlen($this->args['download_name']) > 0) {
						header('Content-Disposition: attachment; filename="'.str_replace('"','\"', $this->args['download_name']).'"');
					} else {
						header('Content-Disposition: attachment');
					}
				}
				break;
			}
		}
		if (!isset($this->path)) {
			debug(__FUNCTION__, 'Unable to find '.$this->args['file']);
			return '404';
		}
	}
	function body() {
		if ($this->extension == 'php') {
			require($this->path);
		} else {
			readfile($this->path);
		}
	}
}
?>
