<?php
namespace WebFrame;
class stylesheet extends Page {
	public static $routing=array('style\\.css' => null);
	function init() {
		contenttype('text/css');
	}
	function body() {
		echo '@charset "utf-8";'."\n";
		foreach (modules() as $module) {
			$dir=cssPath($module);
			if (!isset($dir)) {
				continue;
			}
			foreach (glob($dir.'/*.css') as $file) {
				require($file);
			}
		}
	}
}
?>
