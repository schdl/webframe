<?php
namespace WebFrame;
class users__change_password extends StandardPage {
	public $title='Change Password';
	public static $routing=array('change-password' => null);
	protected $result;
	function init() {
		if (user() === null) {
			return 'login';
		}
		if (isset($_POST['go'])) {
			if (user()->verifyPassword($_POST['old'])) {
				if ($_POST['new1'] == $_POST['new2']) {
					user()->setPassword($_POST['new1']);
					user()->write();
					$this->result='success';
				} else {
					$this->result='nomatch';
				}
			} else {
				$this->result='incorrect';
			}
		}
	}
	function body() {
		echo '<h1>'.$this->title.'</h1>';
		if ($this->result) {
			switch($this->result) {
				case 'incorrect':
					echo print_error('The password you entered is incorrect.');
					break;
				case 'nomatch':
					echo print_warning('The passwords you entered did not match.');
					break;
				case 'success':
					echo print_success('Your password has been changed.');
					break;
			}
		}
		echo '<p><form method="post">Old password: <input name="old" type="password" /><br/>';
		echo 'New password: <input name="new1" type="password" /><br/>';
		echo 'Repeat new password: <input name="new2" type="password" /><br/>';
		echo '<input type="submit" name="go" value="Change Password" /></form></p>';
		echo '<p><a href="'.users__preferences::url().'">Back to Preferences</a></p>';
	}
}
?>
