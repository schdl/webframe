<?php
namespace WebFrame;
class users__forgot_password extends StandardPage {
	public $title='Forgot Password';
	public static $routing=array('forgot' => null);
	function init() {
		if (user() !== null) {
			return array('page' => '302', 'url' => findPage('welcome')->url());
		}
	}
	function body() {
		$form=new form(static::url());
		$form->text('<h1>Reset password</h1>');
		$form->text_input('email', 'email', 'Email');
		$form->submit();
		if (isset($_REQUEST['email'])) {
			$data=$form->process();
			$user=user::fromEmail($data['email']);
			if ($user) {
				$ev=email_verification::create('reset_password', new \DateInterval('P1D'), $data['email'], $user);
				$url=users__reset_password::url('user='.$ev->user.';key='.$ev->key);
				xhtmlemail($user->email, getConf('emailfrom'), getConf('title').' password', 'You requested to reset your '.getConf('title').' password.  To do so, go to <a href="'.$url.'">'.$url.'</a>. This link will expire in twenty-four hours. If you did not request to reset your password, you may safely ignore this message.');
			}
			echo print_success('Success.', 'You have been sent an email (if you have an account at '.getConf('title').') with further instructions to reset your password.');
		} else {
			$form->output();
		}
	}
}
?>
