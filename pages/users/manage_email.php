<?php
namespace WebFrame;
use Forms;
class users__manage_email extends StandardPage {
	public static $routing=array('preferences/email' => null);
	public $title='Manage Email';
	function init() {
		if (!user()) {
			return 'login';
		}
		addScript('dropdown_alert');
		$data=&$this->bodyData;
		$data=['title' => $this->title, 'post_add' => []];
		if (isset($_POST['main']) && strlen($_POST['main'])) {
			$ae=additional_email::select_by_user_AND_email(user(), $_POST['main'])->fetch();
			if ($ae) {
				$tmp=user()->email;
				user()->email=$ae->email;
				$ae->delete();
				$ae=new additional_email();
				$ae->user=user()->id;
				$ae->email=$tmp;
				$ae->write();
				user()->write();
				$ae->write();
				$data['msg']='Your main email address has been changed.';
			}
		} elseif (isset($_POST['delete'])) {
			$ae=additional_email::select_by_user_AND_id(user(), $_POST['delete'])->fetch();
			if ($ae) {
				$data['msg']='The address '.htmlize($ae->email).' has been removed from your account.';
				$ae->delete();
			}
		} elseif (isset($_POST['add'])) {
			if (EmailValidator::create()->validate($_POST['add'])) {
				if ($u=user::fromEmail($_POST['add'])) {
					if ($u->id == user()->id) {
						$data['msg']='The address '.htmlize($_POST['add']).' is already associated with your account.';
					} else {
						$data['msg']='That address is already associated with another account.';
					}
				} else {
					$ev=email_verification::create('additional_email', new \DateInterval('P1D'), $_POST['add'], user());
					xhtmlemail($_POST['add'], null, getConf('title').' email verification', 'To add this email address ('.htmlize($_POST['add']).') to your '.getConf('title').' account, go '.static::link(null, '_get=verify='.$ev->key).'. This link will expire in 24 hours.');
					$data['msg']='An email has been sent to you at '.htmlize($_POST['add']).'. Follow the directions in the email to finish adding the address to your account.';
				}
			} else {
				$data['msg']='The address that you entered is not a valid address.';
			}
		} elseif (isset($_GET['verify']) && ctype_alnum($_GET['verify']) && strlen($_GET['verify']) == 30) {
			$ev=email_verification::validate('additional_email', $_GET['verify'], user());
			if ($ev) {
				$ae=new additional_email();
				$ae->user=user()->id;
				$ae->email=$ev->email;
				$ae->write();
				$ev->delete();
				$data['msg']=htmlize($ev->email).' has been added to your account.';
				$data['post_add']=findPage('include/post_add_email', array('email' => $ev->email), false);
				foreach ($data['post_add'] as $page) {
					$page->init();
				}
			} else {
				$data['msg']='Your verification key was not recognized and may have expired. Please try again.';
			}
		}
		$main_addr_form=&$data['main_addr_form'];
		$additional_email=&$data['additional_email'];
		$new_addr_form=&$data['new_addr_form'];
		$subscriptions_form=&$data['subscriptions_form'];
		$additonal_email=[];
		$main_addr_select=(new Forms\Select())
			->name('main')
			->br(false)
			->default(user()->email)
			->label('This address will be used to send you notifications: ')
			->add(user()->email, user()->email);
		additional_email::select_by_user(user())->each(function($ae) use ($main_addr_select, &$additional_email) {
			$main_addr_select->add($ae->email, $ae->email);
			$additional_email[]=(new Forms\Form())
				->action(self::url())
				->method('post')
				->add((new Forms\Input())
					->type('hidden')
					->name('delete')
					->default($ae->id)
					->label(htmlize($ae->email).' ')
					->br(false))
				->add((new Forms\Submit())
					->text('Delete')
					->br(false));
		});
		$main_addr_form=(new Forms\Form())
			->action(self::url())
			->method('post')
			->add($main_addr_select)
			->add((new Forms\Submit())
				->text('Update')
				->br(false));
		$new_addr_form=(new Forms\Form())
			->action(self::url())
			->method('post')
			->add((new Forms\Input())
				->name('add')
				->label('Add another address: ')
				->br(false))
			->add((new Forms\Submit())
				->text('Add')
				->br(false));
		$subscriptions_form=(new Forms\Form())
			->action(self::url())
			->method('post');
		foreach (Core::$cache['email_prefs'] as $type) {
			(new $type['class'])->addToForm($subscriptions_form);
		}
		$subscriptions_form->add((new Forms\Submit())
			->text('Update Preferences')
			->name('update_prefs'));
		$subs_data=&$this->bodyData['subscriptions_data'];
		$subs_data=$subscriptions_form->val();
		if (isset($subs_data['update_prefs'])) {
			foreach (Core::$cache['email_prefs'] as $type) {
				(new $type['class'])->update($subs_data);
			}
			$data['msg']='Your preferences have been updated.';
		}
	}
}
?>
