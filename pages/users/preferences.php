<?php
namespace WebFrame;
class users__preferences extends StandardPage {
	public $title='User Preferences';
	public static $routing=array('preferences' => null);
	protected $extras;
	function init() {
		if (user() === null) {
			return 'login';
		}
		$this->extras=findPage('include/user_prefs', null, false);
		foreach ($this->extras as $extra) {
			$extra->init();
		}
	}
	function body() {
		echo '<h1>'.$this->title.'</h1>';
		foreach ($this->extras as $extra) {
			$extra->body();
		}
		echo '<p><a href="'.url().'">Home</a></p>';
	}
}
?>
