<?php
namespace WebFrame;
class users__reset_password extends StandardPage {
	public static $routing=array('reset-password/([0-9]+)-([a-zA-Z0-9]{30})' => array('user', 'key'));
	public $title='Reset Password';
	function init() {
		if (user() !== null) {
			return array('page' => '302', 'url' => findPage('welcome')->url());
		}
	}
	function body() {
		echo '<h1>Reset Password</h1>';
		$ev=email_verification::validate('reset_password', $this->args['key'], $this->args['user']);
		if ($ev) {
			$user=$ev->get_user();
			$showform=true;
			if (isset($_POST['password'])) {
				if (strlen($_POST['password']) < 5) {
					echo print_warning('Please choose a password at least 5 characters long.');
				} elseif ($_POST['password'] !== $_POST['confirm']) {
					echo print_warning('The passwords that you entered do not match.');
				} else {
					$user->setPassword($_POST['password']);
					$user->write();
					$showform=false;
					$ev->delete();
					echo print_success('Your password has been changed. Please '.login::link('log in').'.');
				}
			}
			if ($showform) {
				echo '<form method="post">New Password: <input type="password" name="password" /><br/>Confirm Password: <input type="password" name="confirm" /><br/><input type="submit" value="Change Password" /></form>';
			}
		} else {
			echo print_warning('Your reset key was not recognized. It may have expired. Please '.login::link('log in').' or '.users__forgot_password::link('try again').'.');
		}
	}
}
?>
