<?php
namespace WebFrame;
class welcome extends StandardPage {
	public static $routing=array('' => null);
	public static $precedence=1;
	public $title='Welcome';
	function body() {
		echo '<h1>Welcome to '.htmlize(getConf('title')).'</h1>';
	}
	public static function sitemapURLs() {
		return SitemapURL::create(static::url())->changefreq('never');
	}
}
?>
