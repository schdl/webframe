function ajax_reload(data) {
	if (!data) {
		var data={};
	}
	data.ajax=1;
	$.ajax({
		url: window.location.href,
		data: data,
		success: function(data, textStatus, jqXHR) {
			$('#main').html(data);
			for (i=0; i<ajax_reload.callbacks.length; i++) {
				ajax_reload.callbacks[i]();
			}
		},
		dataType: 'html',
		cache: false
	});
}
ajax_reload.callbacks=[]
ajax_reload.register_callback=function (callback) {
	ajax_reload.callbacks.push(callback)
}
