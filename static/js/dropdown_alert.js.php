<?php
namespace WebFrame;
?>
function dropdown_alert(content, duration) {
	if (!duration) {
		var duration=5000;
	}
	$(document).ready(function () {
		var id=Math.floor(Math.random()*1000000);
		$('body').prepend('<div id="a'+id+'" class="alert"><div id="ca'+id+'" class="close_alert"><a id="cab'+id+'" class="close_alert_button" href="javascript:void(0)" title="Close"><img class="inline" src="<?php echo static_content::url('file=images/delete.png'); ?>" alt="[Close]" /></a></div>'+content+'</div>');
		$('#ca'+id).click(function() {
			$('#a'+id).animate({top: -$('#a'+id).outerHeight()}, 500, 'swing', function() {
				$('#a'+id).remove();
			});
		});
		setTimeout(function() {$('#ca'+id).click();}, duration);
		$('#a'+id).css({top: -$('#a'+id).outerHeight(), display: 'block'}).animate({top: 0});
	});
}
