<?php
namespace WebFrame;
?>
function lcat(id, set) { // Short for 'toggle'
	tag=document.getElementById(id);
	for (var i=1; i<tag.childNodes.length; i++) {
		if (tag.childNodes[i].className && tag.childNodes[i].className.indexOf('lcae') != -1) {
			if (typeof(set) == 'undefined') {
				set=tag.childNodes[i].style.display=="none"?"":"none";
			}
			tag.childNodes[i].style.display=set;
		}
	}
}
function lca_expand(id) {
	lcat(id, "");
}
function lca_collapse(id) {
	lcat(id, "none");
}
function lca_search(q, el, depth, maxdepth, t) {
	if (!t) {
		clearTimeout(el.lca_search_timeout);
		el.lca_search_timeout=setTimeout(function () {el.lca_found_last=lca_search(q, el, depth, maxdepth, true)}, 300);
		return;
	}
	if (depth == 0) {
		if (q == el.lca_last_search) return el.lca_found_last;
		if (el.lca_found_last == 0 && q.indexOf(el.lca_last_search) != -1) {
			<?php if (getConf('debug')) { ?>
			console.log({lca: 'Already had no results, not searching \''+q+'\''});
			<?php } ?>
			return 0;
		}
		el.lca_last_search=q;
		<?php if (getConf('debug')) { ?>
		console.log({lca: 'Searching \''+q+'\''});
		<?php } ?>
	}
	var found=0;
	for (var i=0; i<el.childNodes.length; i++) {
		if (el.childNodes[i].nodeName == "LABEL" || (el.childNodes[i].className && el.childNodes[i].className.indexOf('lcal') != -1)) {
			found+=(el.childNodes[i].innerHTML.toLowerCase().indexOf(q.toLowerCase()) == -1?0:1);
			break;
		}
	}
	if (depth < maxdepth) {
		for (var i=0; i<el.childNodes.length; i++) {
			if (!(el.childNodes[i].className && el.childNodes[i].className.indexOf('lcae') != -1)) continue;
			var lfound=lca_search(q, el.childNodes[i], depth+1, maxdepth, true);
			found+=lfound;
			el.childNodes[i].style.display=(q.length == 0 || lfound > 0?"":"none");
		}
		if (q.length == 0 && el.className.indexOf('lcac') != -1) {
			lca_collapse(el.id);
		}
	}
	if (depth == 0) {
		el.childNodes[el.childNodes.length-1].style.display=found?"none":"";
	}
	return found;
}
function lca_show_checked(el, depth, maxdepth) {
	if (depth == 0) {
		el.lca_last_search=undefined;
	}
	var found=0;
	for (var i=0; i<el.childNodes.length; i++) {
		if (el.childNodes[i].nodeName == "INPUT" && el.childNodes[i].type == "checkbox" && el.childNodes[i].checked) {
			found++;
		}
		if (depth < maxdepth) {
			if (!(el.childNodes[i].className && el.childNodes[i].className.indexOf('lcae') != -1)) continue;
			var lfound=lca_show_checked(el.childNodes[i], depth+1, maxdepth);
			found+=lfound;
			el.childNodes[i].style.display=(lfound > 0?"":"none");
		}
	}
	if (depth == 0) {
		el.childNodes[el.childNodes.length-1].style.display=found?"none":"";
	}
	return found;
}
