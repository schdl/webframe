<?php
namespace WebFrame;
?>
<div class="breadcrumb" itemprop="breadcrumb">
<?foreach ($breadcrumbs as $title => $url): ?>
&raquo; <a href="<?=htmlize($url)?>"><?=htmlize($title)?></a>
<?endforeach;?>
</div>
