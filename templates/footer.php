<?php
namespace WebFrame;
if (getConf('debug')) {
	debug('Execution took '.round(elapsedTime(), 3).' seconds and '.round(memory_get_peak_usage(true)/1048576, 3).'MB of RAM');
	runTemplate('debug', array('debug' => Core::$debug));
	Core::$debug=false;
}
?>
</body>
</html>
