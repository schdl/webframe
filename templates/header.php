<?php
namespace WebFrame;
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="<?php echo stylesheet::url() ?>" />
<link rel="icon" href="<?php echo static_content::url('file=images/favicon.png') ?>" />
<?php	if (isset($canonical_url)) { ?>
<link rel="canonical" href="<?php echo htmlize($canonical_url) ?>" />
<?php	}
	foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo htmlize($script) ?>"></script>
<?php	}
	foreach (findPage('include/head', null, false) as $extra) {
		$extra->run();
	}
	if (isset($description)) {
		echo '<meta name="description" content="'.htmlize($description).'" />'."\n";
	}
	if (method_exists(page(), 'head')) {
		page()->head();
	}
	if (isset($title) && count($title)) {
		echo '<title>'.htmlize(implode(' | ', $title).' | '.getConf('title')).'</title>'."\n";
	} else {
		echo '<title>'.htmlize(getConf('title')).'</title>';
	}
?>
<body>
