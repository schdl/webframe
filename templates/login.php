<?php
namespace WebFrame;
runTemplate('topstuff');
if ($please_sign_in) {
	runTemplate('messagebox', array('type' => 'warning', 'msg' => 'Please sign in to access this page.'));
}
if (isset($result)) {
	if ($result) {
		runTemplate('messagebox', array('type' => 'success', 'msg' => 'Welcome, '.htmlize(user()->name()).'.'));
?>
<a href="<?=htmlize(url($go)) ?>">Continue</a>
<?php
	} else {
		runTemplate('messagebox', array('type' => 'error', 'msg' => 'Your email and password combination was not recognized.'));
	}
}
if ($form) {
	runTemplate('form');
?>
<br/>
<a href="<?=findPage('users/forgot_password')->url() ?>">Forgot Password</a> &bull; <a href="<?=findPage('create_account')->url() ?>">Create Account</a>
<? } ?>
