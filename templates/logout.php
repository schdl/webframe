<?php
namespace WebFrame;
runTemplate('messagebox', array('type' => 'success', 'msg' => 'Logged out.'));
if (isset($go)) {
?>
<a href="<?= htmlize(url($go)) ?>">Return to <i><?= htmlize(url($go)) ?></i></a> &bull; 
<? } ?>
<a href="<?= findPage('login')->url() ?>">Log back in</a>
