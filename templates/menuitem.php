<?php
	namespace WebFrame;
	if (isset($title)):?>
<li class="menuitem">
<?		if (isset($href)): ?>
<a class="menutitle" href="<?=htmlize($href)?>"><?=$title?></a>
<?php		elseif (isset($onclick)): ?>
<span class="menutitle pointer" onclick="<?=$onclick?>"><?=$title?></span>
<?php		else: ?>
<span class="menutitle"><?=$title?></span>
<?php		endif; ?>
<?php	endif;
	if (isset($submenu)) {
		runTemplate('menu', array('items' => $submenu));
	} ?>
</li>
