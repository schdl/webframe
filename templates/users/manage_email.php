<?php
namespace WebFrame;
if (isset($msg)):?>
<script type="text/javascript">dropdown_alert(<?=json_encode($msg)?>);</script>
<?endif;
runTemplate('topstuff');
foreach ($post_add as $page) {
	$page->body();
}
?>
<h2>Main Address</h2>
<p><?runTemplate('form', ['form' => $main_addr_form])?></p>
<h2>Additional Addresses</h2>
<p>These addresses can be used to log in, reset your password, and verify your identity.</p>
<?if ($additional_email):?>
<ul>
<?foreach ($additional_email as $form):?>
<li><?runTemplate('form', ['form' => $form]);?></li>
<?endforeach?>
</ul>
<?else:?>
<p>You have no additional email addresses configured.</p>
<?endif?>
<p><?runTemplate('form', ['form' => $new_addr_form])?></p>
<h2>Preferences</h2>
<p><?runTemplate('form', ['form' => $subscriptions_form, 'data' => $subscriptions_data])?></p>
